
package wsdl.enterpriseservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EnterpriseServicePortType", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/enterprise/service/v2")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface EnterpriseServicePortType {


    /**
     * Получение ХС по его глобальному идентификатору.
     *                 Критериями поиска являются глобальный идентификатор и поле last (т.е. запись должна быть последний в
     *                 истории).
     *                 Выполнение операции заканчивается либо возвратом искомого типа продукции,
     *                 либо ошибкой, в случае если запись ХС не найдена или произошла иная ошибка выполнения запроса.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.GetBusinessEntityByGuidResponse
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetBusinessEntityByGuid", action = "GetBusinessEntityByGuid")
    @WebResult(name = "getBusinessEntityByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetBusinessEntityByGuidResponse getBusinessEntityByGuid(
        @WebParam(name = "getBusinessEntityByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetBusinessEntityByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * Получение записи ХС по его идентификатору.
     *                 Выполнение операции заканчивается либо возвратом искомого типа продукции,
     *                 либо ошибкой, в случае если запись ХС не найдена или произошла иная ошибка выполнения запроса.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.GetBusinessEntityByUuidResponse
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetBusinessEntityByUuid", action = "GetBusinessEntityByUuid")
    @WebResult(name = "getBusinessEntityByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetBusinessEntityByUuidResponse getBusinessEntityByUuid(
        @WebParam(name = "getBusinessEntityByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetBusinessEntityByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetBusinessEntityListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetBusinessEntityList", action = "GetBusinessEntityList")
    @WebResult(name = "getBusinessEntityListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetBusinessEntityListResponse getBusinessEntityList(
        @WebParam(name = "getBusinessEntityListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetBusinessEntityListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetBusinessEntityChangesListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetBusinessEntityChangesList", action = "GetBusinessEntityChangesList")
    @WebResult(name = "getBusinessEntityChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetBusinessEntityChangesListResponse getBusinessEntityChangesList(
        @WebParam(name = "getBusinessEntityChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetBusinessEntityChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * Получение ХС по его глобальному идентификатору.
     *                 Критериями поиска являются глобальный идентификатор и поле last (т.е. запись должна быть последний в
     *                 истории).
     *                 Выполнение операции заканчивается либо возвратом искомого типа продукции,
     *                 либо ошибкой, в случае если запись ХС не найдена или произошла иная ошибка выполнения запроса.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.GetEnterpriseByGuidResponse
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetEnterpriseByGuid", action = "GetEnterpriseByGuid")
    @WebResult(name = "getEnterpriseByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetEnterpriseByGuidResponse getEnterpriseByGuid(
        @WebParam(name = "getEnterpriseByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetEnterpriseByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * Получение записи ХС по его идентификатору.
     *                 Выполнение операции заканчивается либо возвратом искомого типа продукции,
     *                 либо ошибкой, в случае если запись ХС не найдена или произошла иная ошибка выполнения запроса.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.GetEnterpriseByUuidResponse
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetEnterpriseByUuid", action = "GetEnterpriseByUuid")
    @WebResult(name = "getEnterpriseByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetEnterpriseByUuidResponse getEnterpriseByUuid(
        @WebParam(name = "getEnterpriseByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetEnterpriseByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetForeignEnterpriseListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetForeignEnterpriseList", action = "GetForeignEnterpriseList")
    @WebResult(name = "getForeignEnterpriseListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetForeignEnterpriseListResponse getForeignEnterpriseList(
        @WebParam(name = "getForeignEnterpriseListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetForeignEnterpriseListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetRussianEnterpriseListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetRussianEnterpriseList", action = "GetRussianEnterpriseList")
    @WebResult(name = "getRussianEnterpriseListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetRussianEnterpriseListResponse getRussianEnterpriseList(
        @WebParam(name = "getRussianEnterpriseListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetRussianEnterpriseListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetForeignEnterpriseChangesListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetForeignEnterpriseChangesList", action = "GetForeignEnterpriseChangesList")
    @WebResult(name = "getForeignEnterpriseChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetForeignEnterpriseChangesListResponse getForeignEnterpriseChangesList(
        @WebParam(name = "getForeignEnterpriseChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetForeignEnterpriseChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetRussianEnterpriseChangesListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetRussianEnterpriseChangesList", action = "GetRussianEnterpriseChangesList")
    @WebResult(name = "getRussianEnterpriseChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetRussianEnterpriseChangesListResponse getRussianEnterpriseChangesList(
        @WebParam(name = "getRussianEnterpriseChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetRussianEnterpriseChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * Получение пары ХС-площадка по её глобальному идентификатору GLN.
     *                 Выполнение операции заканчивается либо возвратом искомой пары,
     *                 либо ошибкой, в случае если запись с указанным GLN не найдена или произошла иная ошибка выполнения запроса.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.GetBusinessMemberByGLNResponse
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetBusinessMemberByGLN", action = "GetBusinessMemberByGLN")
    @WebResult(name = "getBusinessMemberByGLNResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetBusinessMemberByGLNResponse getBusinessMemberByGLN(
        @WebParam(name = "getBusinessMemberByGLNRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetBusinessMemberByGLNRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.GetActivityLocationListResponse
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetActivityLocationList", action = "GetActivityLocationList")
    @WebResult(name = "getActivityLocationListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetActivityLocationListResponse getActivityLocationList(
        @WebParam(name = "getActivityLocationListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetActivityLocationListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

}
