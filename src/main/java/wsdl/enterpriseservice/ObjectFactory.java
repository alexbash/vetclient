
package wsdl.enterpriseservice;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOptions_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "listOptions");
    private final static QName _BusinessEntityList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "businessEntityList");
    private final static QName _Uuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "uuid");
    private final static QName _DistrictGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "districtGuid");
    private final static QName _RequestRejectedFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "requestRejectedFault");
    private final static QName _R13NShippingRuleList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nShippingRuleList");
    private final static QName _InternalServiceFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "internalServiceFault");
    private final static QName _IncorrectRequestFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "incorrectRequestFault");
    private final static QName _RegionList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "regionList");
    private final static QName _R13NConditionList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nConditionList");
    private final static QName _Product_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "product");
    private final static QName _Disease_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "disease");
    private final static QName _ProductType_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productType");
    private final static QName _R13NRegionStatusList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nRegionStatusList");
    private final static QName _ResearchMethodList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "researchMethodList");
    private final static QName _BusinessMember_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "businessMember");
    private final static QName _ActivityLocationList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "activityLocationList");
    private final static QName _EnterpriseList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterpriseList");
    private final static QName _SubProductList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "subProductList");
    private final static QName _ResearchMethod_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "researchMethod");
    private final static QName _BusinessEntity_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "businessEntity");
    private final static QName _UnitList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "unitList");
    private final static QName _RegionGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "regionGuid");
    private final static QName _CountryGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "countryGuid");
    private final static QName _GlobalID_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "globalID");
    private final static QName _Enterprise_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterprise");
    private final static QName _EnterpriseGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterpriseGuid");
    private final static QName _Guid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "guid");
    private final static QName _UpdateDateInterval_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "updateDateInterval");
    private final static QName _Purpose_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "purpose");
    private final static QName _CargoType_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "cargoType");
    private final static QName _ProductList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productList");
    private final static QName _Region_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "region");
    private final static QName _PurposeList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "purposeList");
    private final static QName _AccessDeniedFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "accessDeniedFault");
    private final static QName _LocalityList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "localityList");
    private final static QName _StreetList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "streetList");
    private final static QName _SubProductGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "subProductGuid");
    private final static QName _District_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "district");
    private final static QName _Unit_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "unit");
    private final static QName _OffsetOutOfRangeFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "offsetOutOfRangeFault");
    private final static QName _ProductItem_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productItem");
    private final static QName _SubProduct_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "subProduct");
    private final static QName _R13NZone_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nZone");
    private final static QName _CountryList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "countryList");
    private final static QName _EntityNotFoundFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "entityNotFoundFault");
    private final static QName _EnterpriseGroup_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterpriseGroup");
    private final static QName _LocalityGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "localityGuid");
    private final static QName _DistrictList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "districtList");
    private final static QName _DiseaseList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "diseaseList");
    private final static QName _ProductItemList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productItemList");
    private final static QName _ProductGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productGuid");
    private final static QName _Country_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "country");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BusinessEntity }
     * 
     */
    public BusinessEntity createBusinessEntity() {
        return new BusinessEntity();
    }

    /**
     * Create an instance of {@link FaultInfo }
     * 
     */
    public FaultInfo createFaultInfo() {
        return new FaultInfo();
    }

    /**
     * Create an instance of {@link ListOptions }
     * 
     */
    public ListOptions createListOptions() {
        return new ListOptions();
    }

    /**
     * Create an instance of {@link DateInterval }
     * 
     */
    public DateInterval createDateInterval() {
        return new DateInterval();
    }

    /**
     * Create an instance of {@link EntityList }
     * 
     */
    public EntityList createEntityList() {
        return new EntityList();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link GenericEntity }
     * 
     */
    public GenericEntity createGenericEntity() {
        return new GenericEntity();
    }

    /**
     * Create an instance of {@link GenericVersioningEntity }
     * 
     */
    public GenericVersioningEntity createGenericVersioningEntity() {
        return new GenericVersioningEntity();
    }

    /**
     * Create an instance of {@link GetEnterpriseByUuidResponse }
     * 
     */
    public GetEnterpriseByUuidResponse createGetEnterpriseByUuidResponse() {
        return new GetEnterpriseByUuidResponse();
    }

    /**
     * Create an instance of {@link Enterprise }
     * 
     */
    public Enterprise createEnterprise() {
        return new Enterprise();
    }

    /**
     * Create an instance of {@link FindStreetListByNameResponse }
     * 
     */
    public FindStreetListByNameResponse createFindStreetListByNameResponse() {
        return new FindStreetListByNameResponse();
    }

    /**
     * Create an instance of {@link StreetList }
     * 
     */
    public StreetList createStreetList() {
        return new StreetList();
    }

    /**
     * Create an instance of {@link GetBusinessEntityByGuidRequest }
     * 
     */
    public GetBusinessEntityByGuidRequest createGetBusinessEntityByGuidRequest() {
        return new GetBusinessEntityByGuidRequest();
    }

    /**
     * Create an instance of {@link GetSubProductByProductListRequest }
     * 
     */
    public GetSubProductByProductListRequest createGetSubProductByProductListRequest() {
        return new GetSubProductByProductListRequest();
    }

    /**
     * Create an instance of {@link GetResearchMethodListResponse }
     * 
     */
    public GetResearchMethodListResponse createGetResearchMethodListResponse() {
        return new GetResearchMethodListResponse();
    }

    /**
     * Create an instance of {@link ResearchMethodList }
     * 
     */
    public ResearchMethodList createResearchMethodList() {
        return new ResearchMethodList();
    }

    /**
     * Create an instance of {@link GetDistrictByUuidResponse }
     * 
     */
    public GetDistrictByUuidResponse createGetDistrictByUuidResponse() {
        return new GetDistrictByUuidResponse();
    }

    /**
     * Create an instance of {@link District }
     * 
     */
    public District createDistrict() {
        return new District();
    }

    /**
     * Create an instance of {@link GetActualR13NShippingRuleListResponse }
     * 
     */
    public GetActualR13NShippingRuleListResponse createGetActualR13NShippingRuleListResponse() {
        return new GetActualR13NShippingRuleListResponse();
    }

    /**
     * Create an instance of {@link RegionalizationShippingRuleList }
     * 
     */
    public RegionalizationShippingRuleList createRegionalizationShippingRuleList() {
        return new RegionalizationShippingRuleList();
    }

    /**
     * Create an instance of {@link GetPurposeByUuidResponse }
     * 
     */
    public GetPurposeByUuidResponse createGetPurposeByUuidResponse() {
        return new GetPurposeByUuidResponse();
    }

    /**
     * Create an instance of {@link Purpose }
     * 
     */
    public Purpose createPurpose() {
        return new Purpose();
    }

    /**
     * Create an instance of {@link GetStreetListByLocalityRequest }
     * 
     */
    public GetStreetListByLocalityRequest createGetStreetListByLocalityRequest() {
        return new GetStreetListByLocalityRequest();
    }

    /**
     * Create an instance of {@link GetForeignEnterpriseChangesListResponse }
     * 
     */
    public GetForeignEnterpriseChangesListResponse createGetForeignEnterpriseChangesListResponse() {
        return new GetForeignEnterpriseChangesListResponse();
    }

    /**
     * Create an instance of {@link EnterpriseList }
     * 
     */
    public EnterpriseList createEnterpriseList() {
        return new EnterpriseList();
    }

    /**
     * Create an instance of {@link GetSubProductByUuidResponse }
     * 
     */
    public GetSubProductByUuidResponse createGetSubProductByUuidResponse() {
        return new GetSubProductByUuidResponse();
    }

    /**
     * Create an instance of {@link SubProduct }
     * 
     */
    public SubProduct createSubProduct() {
        return new SubProduct();
    }

    /**
     * Create an instance of {@link GetResearchMethodByUuidResponse }
     * 
     */
    public GetResearchMethodByUuidResponse createGetResearchMethodByUuidResponse() {
        return new GetResearchMethodByUuidResponse();
    }

    /**
     * Create an instance of {@link ResearchMethod }
     * 
     */
    public ResearchMethod createResearchMethod() {
        return new ResearchMethod();
    }

    /**
     * Create an instance of {@link GetBusinessEntityListResponse }
     * 
     */
    public GetBusinessEntityListResponse createGetBusinessEntityListResponse() {
        return new GetBusinessEntityListResponse();
    }

    /**
     * Create an instance of {@link BusinessEntityList }
     * 
     */
    public BusinessEntityList createBusinessEntityList() {
        return new BusinessEntityList();
    }

    /**
     * Create an instance of {@link GetR13NConditionListResponse }
     * 
     */
    public GetR13NConditionListResponse createGetR13NConditionListResponse() {
        return new GetR13NConditionListResponse();
    }

    /**
     * Create an instance of {@link RegionalizationConditionList }
     * 
     */
    public RegionalizationConditionList createRegionalizationConditionList() {
        return new RegionalizationConditionList();
    }

    /**
     * Create an instance of {@link GetUnitListResponse }
     * 
     */
    public GetUnitListResponse createGetUnitListResponse() {
        return new GetUnitListResponse();
    }

    /**
     * Create an instance of {@link UnitList }
     * 
     */
    public UnitList createUnitList() {
        return new UnitList();
    }

    /**
     * Create an instance of {@link GetRegionByGuidResponse }
     * 
     */
    public GetRegionByGuidResponse createGetRegionByGuidResponse() {
        return new GetRegionByGuidResponse();
    }

    /**
     * Create an instance of {@link Region }
     * 
     */
    public Region createRegion() {
        return new Region();
    }

    /**
     * Create an instance of {@link GetProductByUuidResponse }
     * 
     */
    public GetProductByUuidResponse createGetProductByUuidResponse() {
        return new GetProductByUuidResponse();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link FindStreetListByNameRequest }
     * 
     */
    public FindStreetListByNameRequest createFindStreetListByNameRequest() {
        return new FindStreetListByNameRequest();
    }

    /**
     * Create an instance of {@link GetSubProductByUuidRequest }
     * 
     */
    public GetSubProductByUuidRequest createGetSubProductByUuidRequest() {
        return new GetSubProductByUuidRequest();
    }

    /**
     * Create an instance of {@link GetSubProductChangesListResponse }
     * 
     */
    public GetSubProductChangesListResponse createGetSubProductChangesListResponse() {
        return new GetSubProductChangesListResponse();
    }

    /**
     * Create an instance of {@link SubProductList }
     * 
     */
    public SubProductList createSubProductList() {
        return new SubProductList();
    }

    /**
     * Create an instance of {@link GetUnitListRequest }
     * 
     */
    public GetUnitListRequest createGetUnitListRequest() {
        return new GetUnitListRequest();
    }

    /**
     * Create an instance of {@link GetDiseaseChangesListResponse }
     * 
     */
    public GetDiseaseChangesListResponse createGetDiseaseChangesListResponse() {
        return new GetDiseaseChangesListResponse();
    }

    /**
     * Create an instance of {@link AnimalDiseaseList }
     * 
     */
    public AnimalDiseaseList createAnimalDiseaseList() {
        return new AnimalDiseaseList();
    }

    /**
     * Create an instance of {@link GetSubProductByGuidRequest }
     * 
     */
    public GetSubProductByGuidRequest createGetSubProductByGuidRequest() {
        return new GetSubProductByGuidRequest();
    }

    /**
     * Create an instance of {@link GetPurposeListRequest }
     * 
     */
    public GetPurposeListRequest createGetPurposeListRequest() {
        return new GetPurposeListRequest();
    }

    /**
     * Create an instance of {@link GetRussianEnterpriseChangesListResponse }
     * 
     */
    public GetRussianEnterpriseChangesListResponse createGetRussianEnterpriseChangesListResponse() {
        return new GetRussianEnterpriseChangesListResponse();
    }

    /**
     * Create an instance of {@link GetSubProductByProductListResponse }
     * 
     */
    public GetSubProductByProductListResponse createGetSubProductByProductListResponse() {
        return new GetSubProductByProductListResponse();
    }

    /**
     * Create an instance of {@link GetDiseaseByUuidRequest }
     * 
     */
    public GetDiseaseByUuidRequest createGetDiseaseByUuidRequest() {
        return new GetDiseaseByUuidRequest();
    }

    /**
     * Create an instance of {@link GetProductItemListRequest }
     * 
     */
    public GetProductItemListRequest createGetProductItemListRequest() {
        return new GetProductItemListRequest();
    }

    /**
     * Create an instance of {@link GetDistrictListByRegionRequest }
     * 
     */
    public GetDistrictListByRegionRequest createGetDistrictListByRegionRequest() {
        return new GetDistrictListByRegionRequest();
    }

    /**
     * Create an instance of {@link GetBusinessMemberByGLNResponse }
     * 
     */
    public GetBusinessMemberByGLNResponse createGetBusinessMemberByGLNResponse() {
        return new GetBusinessMemberByGLNResponse();
    }

    /**
     * Create an instance of {@link BusinessMember }
     * 
     */
    public BusinessMember createBusinessMember() {
        return new BusinessMember();
    }

    /**
     * Create an instance of {@link GetActivityLocationListRequest }
     * 
     */
    public GetActivityLocationListRequest createGetActivityLocationListRequest() {
        return new GetActivityLocationListRequest();
    }

    /**
     * Create an instance of {@link GetUnitByGuidRequest }
     * 
     */
    public GetUnitByGuidRequest createGetUnitByGuidRequest() {
        return new GetUnitByGuidRequest();
    }

    /**
     * Create an instance of {@link GetActualR13NShippingRuleListRequest }
     * 
     */
    public GetActualR13NShippingRuleListRequest createGetActualR13NShippingRuleListRequest() {
        return new GetActualR13NShippingRuleListRequest();
    }

    /**
     * Create an instance of {@link AnimalDisease }
     * 
     */
    public AnimalDisease createAnimalDisease() {
        return new AnimalDisease();
    }

    /**
     * Create an instance of {@link GetEnterpriseByUuidRequest }
     * 
     */
    public GetEnterpriseByUuidRequest createGetEnterpriseByUuidRequest() {
        return new GetEnterpriseByUuidRequest();
    }

    /**
     * Create an instance of {@link GetBusinessEntityChangesListResponse }
     * 
     */
    public GetBusinessEntityChangesListResponse createGetBusinessEntityChangesListResponse() {
        return new GetBusinessEntityChangesListResponse();
    }

    /**
     * Create an instance of {@link GetDiseaseByGuidRequest }
     * 
     */
    public GetDiseaseByGuidRequest createGetDiseaseByGuidRequest() {
        return new GetDiseaseByGuidRequest();
    }

    /**
     * Create an instance of {@link GetEnterpriseByGuidRequest }
     * 
     */
    public GetEnterpriseByGuidRequest createGetEnterpriseByGuidRequest() {
        return new GetEnterpriseByGuidRequest();
    }

    /**
     * Create an instance of {@link GetResearchMethodChangesListRequest }
     * 
     */
    public GetResearchMethodChangesListRequest createGetResearchMethodChangesListRequest() {
        return new GetResearchMethodChangesListRequest();
    }

    /**
     * Create an instance of {@link GetDistrictChangesListResponse }
     * 
     */
    public GetDistrictChangesListResponse createGetDistrictChangesListResponse() {
        return new GetDistrictChangesListResponse();
    }

    /**
     * Create an instance of {@link DistrictList }
     * 
     */
    public DistrictList createDistrictList() {
        return new DistrictList();
    }

    /**
     * Create an instance of {@link GetDiseaseListResponse }
     * 
     */
    public GetDiseaseListResponse createGetDiseaseListResponse() {
        return new GetDiseaseListResponse();
    }

    /**
     * Create an instance of {@link GetBusinessEntityChangesListRequest }
     * 
     */
    public GetBusinessEntityChangesListRequest createGetBusinessEntityChangesListRequest() {
        return new GetBusinessEntityChangesListRequest();
    }

    /**
     * Create an instance of {@link GetPurposeListResponse }
     * 
     */
    public GetPurposeListResponse createGetPurposeListResponse() {
        return new GetPurposeListResponse();
    }

    /**
     * Create an instance of {@link PurposeList }
     * 
     */
    public PurposeList createPurposeList() {
        return new PurposeList();
    }

    /**
     * Create an instance of {@link GetDiseaseByGuidResponse }
     * 
     */
    public GetDiseaseByGuidResponse createGetDiseaseByGuidResponse() {
        return new GetDiseaseByGuidResponse();
    }

    /**
     * Create an instance of {@link GetUnitByUuidRequest }
     * 
     */
    public GetUnitByUuidRequest createGetUnitByUuidRequest() {
        return new GetUnitByUuidRequest();
    }

    /**
     * Create an instance of {@link GetRegionListByCountryRequest }
     * 
     */
    public GetRegionListByCountryRequest createGetRegionListByCountryRequest() {
        return new GetRegionListByCountryRequest();
    }

    /**
     * Create an instance of {@link GetLocalityListByDistrictRequest }
     * 
     */
    public GetLocalityListByDistrictRequest createGetLocalityListByDistrictRequest() {
        return new GetLocalityListByDistrictRequest();
    }

    /**
     * Create an instance of {@link GetBusinessEntityByUuidRequest }
     * 
     */
    public GetBusinessEntityByUuidRequest createGetBusinessEntityByUuidRequest() {
        return new GetBusinessEntityByUuidRequest();
    }

    /**
     * Create an instance of {@link GetProductChangesListRequest }
     * 
     */
    public GetProductChangesListRequest createGetProductChangesListRequest() {
        return new GetProductChangesListRequest();
    }

    /**
     * Create an instance of {@link GetRegionListByCountryResponse }
     * 
     */
    public GetRegionListByCountryResponse createGetRegionListByCountryResponse() {
        return new GetRegionListByCountryResponse();
    }

    /**
     * Create an instance of {@link RegionList }
     * 
     */
    public RegionList createRegionList() {
        return new RegionList();
    }

    /**
     * Create an instance of {@link GetBusinessMemberByGLNRequest }
     * 
     */
    public GetBusinessMemberByGLNRequest createGetBusinessMemberByGLNRequest() {
        return new GetBusinessMemberByGLNRequest();
    }

    /**
     * Create an instance of {@link GetRussianEnterpriseChangesListRequest }
     * 
     */
    public GetRussianEnterpriseChangesListRequest createGetRussianEnterpriseChangesListRequest() {
        return new GetRussianEnterpriseChangesListRequest();
    }

    /**
     * Create an instance of {@link GetCountryChangesListRequest }
     * 
     */
    public GetCountryChangesListRequest createGetCountryChangesListRequest() {
        return new GetCountryChangesListRequest();
    }

    /**
     * Create an instance of {@link GetActivityLocationListResponse }
     * 
     */
    public GetActivityLocationListResponse createGetActivityLocationListResponse() {
        return new GetActivityLocationListResponse();
    }

    /**
     * Create an instance of {@link ActivityLocationList }
     * 
     */
    public ActivityLocationList createActivityLocationList() {
        return new ActivityLocationList();
    }

    /**
     * Create an instance of {@link GetProductByTypeListResponse }
     * 
     */
    public GetProductByTypeListResponse createGetProductByTypeListResponse() {
        return new GetProductByTypeListResponse();
    }

    /**
     * Create an instance of {@link ProductList }
     * 
     */
    public ProductList createProductList() {
        return new ProductList();
    }

    /**
     * Create an instance of {@link GetDistrictByGuidResponse }
     * 
     */
    public GetDistrictByGuidResponse createGetDistrictByGuidResponse() {
        return new GetDistrictByGuidResponse();
    }

    /**
     * Create an instance of {@link GetDiseaseListRequest }
     * 
     */
    public GetDiseaseListRequest createGetDiseaseListRequest() {
        return new GetDiseaseListRequest();
    }

    /**
     * Create an instance of {@link GetDistrictListByRegionResponse }
     * 
     */
    public GetDistrictListByRegionResponse createGetDistrictListByRegionResponse() {
        return new GetDistrictListByRegionResponse();
    }

    /**
     * Create an instance of {@link GetProductItemChangesListRequest }
     * 
     */
    public GetProductItemChangesListRequest createGetProductItemChangesListRequest() {
        return new GetProductItemChangesListRequest();
    }

    /**
     * Create an instance of {@link GetDiseaseChangesListRequest }
     * 
     */
    public GetDiseaseChangesListRequest createGetDiseaseChangesListRequest() {
        return new GetDiseaseChangesListRequest();
    }

    /**
     * Create an instance of {@link GetEnterpriseByGuidResponse }
     * 
     */
    public GetEnterpriseByGuidResponse createGetEnterpriseByGuidResponse() {
        return new GetEnterpriseByGuidResponse();
    }

    /**
     * Create an instance of {@link GetLocalityListByDistrictResponse }
     * 
     */
    public GetLocalityListByDistrictResponse createGetLocalityListByDistrictResponse() {
        return new GetLocalityListByDistrictResponse();
    }

    /**
     * Create an instance of {@link LocalityList }
     * 
     */
    public LocalityList createLocalityList() {
        return new LocalityList();
    }

    /**
     * Create an instance of {@link GetRegionByUuidResponse }
     * 
     */
    public GetRegionByUuidResponse createGetRegionByUuidResponse() {
        return new GetRegionByUuidResponse();
    }

    /**
     * Create an instance of {@link GetBusinessEntityListRequest }
     * 
     */
    public GetBusinessEntityListRequest createGetBusinessEntityListRequest() {
        return new GetBusinessEntityListRequest();
    }

    /**
     * Create an instance of {@link GetDiseaseByUuidResponse }
     * 
     */
    public GetDiseaseByUuidResponse createGetDiseaseByUuidResponse() {
        return new GetDiseaseByUuidResponse();
    }

    /**
     * Create an instance of {@link GetDistrictChangesListRequest }
     * 
     */
    public GetDistrictChangesListRequest createGetDistrictChangesListRequest() {
        return new GetDistrictChangesListRequest();
    }

    /**
     * Create an instance of {@link FindLocalityListByNameRequest }
     * 
     */
    public FindLocalityListByNameRequest createFindLocalityListByNameRequest() {
        return new FindLocalityListByNameRequest();
    }

    /**
     * Create an instance of {@link GetActualR13NRegionStatusListResponse }
     * 
     */
    public GetActualR13NRegionStatusListResponse createGetActualR13NRegionStatusListResponse() {
        return new GetActualR13NRegionStatusListResponse();
    }

    /**
     * Create an instance of {@link RegionalizationRegionStatusList }
     * 
     */
    public RegionalizationRegionStatusList createRegionalizationRegionStatusList() {
        return new RegionalizationRegionStatusList();
    }

    /**
     * Create an instance of {@link GetRegionByGuidRequest }
     * 
     */
    public GetRegionByGuidRequest createGetRegionByGuidRequest() {
        return new GetRegionByGuidRequest();
    }

    /**
     * Create an instance of {@link GetStreetListByLocalityResponse }
     * 
     */
    public GetStreetListByLocalityResponse createGetStreetListByLocalityResponse() {
        return new GetStreetListByLocalityResponse();
    }

    /**
     * Create an instance of {@link GetActualR13NRegionStatusListRequest }
     * 
     */
    public GetActualR13NRegionStatusListRequest createGetActualR13NRegionStatusListRequest() {
        return new GetActualR13NRegionStatusListRequest();
    }

    /**
     * Create an instance of {@link Area }
     * 
     */
    public Area createArea() {
        return new Area();
    }

    /**
     * Create an instance of {@link GetProductByUuidRequest }
     * 
     */
    public GetProductByUuidRequest createGetProductByUuidRequest() {
        return new GetProductByUuidRequest();
    }

    /**
     * Create an instance of {@link GetCountryByUuidRequest }
     * 
     */
    public GetCountryByUuidRequest createGetCountryByUuidRequest() {
        return new GetCountryByUuidRequest();
    }

    /**
     * Create an instance of {@link GetPurposeByUuidRequest }
     * 
     */
    public GetPurposeByUuidRequest createGetPurposeByUuidRequest() {
        return new GetPurposeByUuidRequest();
    }

    /**
     * Create an instance of {@link GetPurposeChangesListRequest }
     * 
     */
    public GetPurposeChangesListRequest createGetPurposeChangesListRequest() {
        return new GetPurposeChangesListRequest();
    }

    /**
     * Create an instance of {@link GetProductItemByUuidRequest }
     * 
     */
    public GetProductItemByUuidRequest createGetProductItemByUuidRequest() {
        return new GetProductItemByUuidRequest();
    }

    /**
     * Create an instance of {@link GetProductByTypeListRequest }
     * 
     */
    public GetProductByTypeListRequest createGetProductByTypeListRequest() {
        return new GetProductByTypeListRequest();
    }

    /**
     * Create an instance of {@link GetCountryByUuidResponse }
     * 
     */
    public GetCountryByUuidResponse createGetCountryByUuidResponse() {
        return new GetCountryByUuidResponse();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link GetResearchMethodByGuidRequest }
     * 
     */
    public GetResearchMethodByGuidRequest createGetResearchMethodByGuidRequest() {
        return new GetResearchMethodByGuidRequest();
    }

    /**
     * Create an instance of {@link GetRussianEnterpriseListRequest }
     * 
     */
    public GetRussianEnterpriseListRequest createGetRussianEnterpriseListRequest() {
        return new GetRussianEnterpriseListRequest();
    }

    /**
     * Create an instance of {@link GetForeignEnterpriseListResponse }
     * 
     */
    public GetForeignEnterpriseListResponse createGetForeignEnterpriseListResponse() {
        return new GetForeignEnterpriseListResponse();
    }

    /**
     * Create an instance of {@link GetRegionByUuidRequest }
     * 
     */
    public GetRegionByUuidRequest createGetRegionByUuidRequest() {
        return new GetRegionByUuidRequest();
    }

    /**
     * Create an instance of {@link GetProductByGuidResponse }
     * 
     */
    public GetProductByGuidResponse createGetProductByGuidResponse() {
        return new GetProductByGuidResponse();
    }

    /**
     * Create an instance of {@link GetProductItemByGuidRequest }
     * 
     */
    public GetProductItemByGuidRequest createGetProductItemByGuidRequest() {
        return new GetProductItemByGuidRequest();
    }

    /**
     * Create an instance of {@link GetPurposeChangesListResponse }
     * 
     */
    public GetPurposeChangesListResponse createGetPurposeChangesListResponse() {
        return new GetPurposeChangesListResponse();
    }

    /**
     * Create an instance of {@link GetCountryByGuidRequest }
     * 
     */
    public GetCountryByGuidRequest createGetCountryByGuidRequest() {
        return new GetCountryByGuidRequest();
    }

    /**
     * Create an instance of {@link GetSubProductChangesListRequest }
     * 
     */
    public GetSubProductChangesListRequest createGetSubProductChangesListRequest() {
        return new GetSubProductChangesListRequest();
    }

    /**
     * Create an instance of {@link GetLocalityListByLocalityRequest }
     * 
     */
    public GetLocalityListByLocalityRequest createGetLocalityListByLocalityRequest() {
        return new GetLocalityListByLocalityRequest();
    }

    /**
     * Create an instance of {@link GetPurposeByGuidRequest }
     * 
     */
    public GetPurposeByGuidRequest createGetPurposeByGuidRequest() {
        return new GetPurposeByGuidRequest();
    }

    /**
     * Create an instance of {@link GetPurposeByGuidResponse }
     * 
     */
    public GetPurposeByGuidResponse createGetPurposeByGuidResponse() {
        return new GetPurposeByGuidResponse();
    }

    /**
     * Create an instance of {@link GetResearchMethodByGuidResponse }
     * 
     */
    public GetResearchMethodByGuidResponse createGetResearchMethodByGuidResponse() {
        return new GetResearchMethodByGuidResponse();
    }

    /**
     * Create an instance of {@link GetProductItemByUuidResponse }
     * 
     */
    public GetProductItemByUuidResponse createGetProductItemByUuidResponse() {
        return new GetProductItemByUuidResponse();
    }

    /**
     * Create an instance of {@link ProductItem }
     * 
     */
    public ProductItem createProductItem() {
        return new ProductItem();
    }

    /**
     * Create an instance of {@link GetLocalityListByLocalityResponse }
     * 
     */
    public GetLocalityListByLocalityResponse createGetLocalityListByLocalityResponse() {
        return new GetLocalityListByLocalityResponse();
    }

    /**
     * Create an instance of {@link GetLocalityListByRegionResponse }
     * 
     */
    public GetLocalityListByRegionResponse createGetLocalityListByRegionResponse() {
        return new GetLocalityListByRegionResponse();
    }

    /**
     * Create an instance of {@link GetDistrictByGuidRequest }
     * 
     */
    public GetDistrictByGuidRequest createGetDistrictByGuidRequest() {
        return new GetDistrictByGuidRequest();
    }

    /**
     * Create an instance of {@link GetDistrictByUuidRequest }
     * 
     */
    public GetDistrictByUuidRequest createGetDistrictByUuidRequest() {
        return new GetDistrictByUuidRequest();
    }

    /**
     * Create an instance of {@link GetProductItemByGuidResponse }
     * 
     */
    public GetProductItemByGuidResponse createGetProductItemByGuidResponse() {
        return new GetProductItemByGuidResponse();
    }

    /**
     * Create an instance of {@link GetUnitChangesListResponse }
     * 
     */
    public GetUnitChangesListResponse createGetUnitChangesListResponse() {
        return new GetUnitChangesListResponse();
    }

    /**
     * Create an instance of {@link GetResearchMethodListRequest }
     * 
     */
    public GetResearchMethodListRequest createGetResearchMethodListRequest() {
        return new GetResearchMethodListRequest();
    }

    /**
     * Create an instance of {@link GetProductByGuidRequest }
     * 
     */
    public GetProductByGuidRequest createGetProductByGuidRequest() {
        return new GetProductByGuidRequest();
    }

    /**
     * Create an instance of {@link GetForeignEnterpriseChangesListRequest }
     * 
     */
    public GetForeignEnterpriseChangesListRequest createGetForeignEnterpriseChangesListRequest() {
        return new GetForeignEnterpriseChangesListRequest();
    }

    /**
     * Create an instance of {@link GetProductItemChangesListResponse }
     * 
     */
    public GetProductItemChangesListResponse createGetProductItemChangesListResponse() {
        return new GetProductItemChangesListResponse();
    }

    /**
     * Create an instance of {@link ProductItemList }
     * 
     */
    public ProductItemList createProductItemList() {
        return new ProductItemList();
    }

    /**
     * Create an instance of {@link GetAllCountryListRequest }
     * 
     */
    public GetAllCountryListRequest createGetAllCountryListRequest() {
        return new GetAllCountryListRequest();
    }

    /**
     * Create an instance of {@link GetRegionChangesListRequest }
     * 
     */
    public GetRegionChangesListRequest createGetRegionChangesListRequest() {
        return new GetRegionChangesListRequest();
    }

    /**
     * Create an instance of {@link GetResearchMethodByUuidRequest }
     * 
     */
    public GetResearchMethodByUuidRequest createGetResearchMethodByUuidRequest() {
        return new GetResearchMethodByUuidRequest();
    }

    /**
     * Create an instance of {@link GetAllCountryListResponse }
     * 
     */
    public GetAllCountryListResponse createGetAllCountryListResponse() {
        return new GetAllCountryListResponse();
    }

    /**
     * Create an instance of {@link CountryList }
     * 
     */
    public CountryList createCountryList() {
        return new CountryList();
    }

    /**
     * Create an instance of {@link GetRussianEnterpriseListResponse }
     * 
     */
    public GetRussianEnterpriseListResponse createGetRussianEnterpriseListResponse() {
        return new GetRussianEnterpriseListResponse();
    }

    /**
     * Create an instance of {@link GetUnitByGuidResponse }
     * 
     */
    public GetUnitByGuidResponse createGetUnitByGuidResponse() {
        return new GetUnitByGuidResponse();
    }

    /**
     * Create an instance of {@link Unit }
     * 
     */
    public Unit createUnit() {
        return new Unit();
    }

    /**
     * Create an instance of {@link GetCountryByGuidResponse }
     * 
     */
    public GetCountryByGuidResponse createGetCountryByGuidResponse() {
        return new GetCountryByGuidResponse();
    }

    /**
     * Create an instance of {@link GetBusinessEntityByGuidResponse }
     * 
     */
    public GetBusinessEntityByGuidResponse createGetBusinessEntityByGuidResponse() {
        return new GetBusinessEntityByGuidResponse();
    }

    /**
     * Create an instance of {@link GetSubProductByGuidResponse }
     * 
     */
    public GetSubProductByGuidResponse createGetSubProductByGuidResponse() {
        return new GetSubProductByGuidResponse();
    }

    /**
     * Create an instance of {@link FindLocalityListByNameResponse }
     * 
     */
    public FindLocalityListByNameResponse createFindLocalityListByNameResponse() {
        return new FindLocalityListByNameResponse();
    }

    /**
     * Create an instance of {@link GetR13NConditionListRequest }
     * 
     */
    public GetR13NConditionListRequest createGetR13NConditionListRequest() {
        return new GetR13NConditionListRequest();
    }

    /**
     * Create an instance of {@link GetCountryChangesListResponse }
     * 
     */
    public GetCountryChangesListResponse createGetCountryChangesListResponse() {
        return new GetCountryChangesListResponse();
    }

    /**
     * Create an instance of {@link GetUnitChangesListRequest }
     * 
     */
    public GetUnitChangesListRequest createGetUnitChangesListRequest() {
        return new GetUnitChangesListRequest();
    }

    /**
     * Create an instance of {@link GetRegionChangesListResponse }
     * 
     */
    public GetRegionChangesListResponse createGetRegionChangesListResponse() {
        return new GetRegionChangesListResponse();
    }

    /**
     * Create an instance of {@link GetForeignEnterpriseListRequest }
     * 
     */
    public GetForeignEnterpriseListRequest createGetForeignEnterpriseListRequest() {
        return new GetForeignEnterpriseListRequest();
    }

    /**
     * Create an instance of {@link GetLocalityListByRegionRequest }
     * 
     */
    public GetLocalityListByRegionRequest createGetLocalityListByRegionRequest() {
        return new GetLocalityListByRegionRequest();
    }

    /**
     * Create an instance of {@link GetProductChangesListResponse }
     * 
     */
    public GetProductChangesListResponse createGetProductChangesListResponse() {
        return new GetProductChangesListResponse();
    }

    /**
     * Create an instance of {@link GetBusinessEntityByUuidResponse }
     * 
     */
    public GetBusinessEntityByUuidResponse createGetBusinessEntityByUuidResponse() {
        return new GetBusinessEntityByUuidResponse();
    }

    /**
     * Create an instance of {@link GetProductItemListResponse }
     * 
     */
    public GetProductItemListResponse createGetProductItemListResponse() {
        return new GetProductItemListResponse();
    }

    /**
     * Create an instance of {@link GetUnitByUuidResponse }
     * 
     */
    public GetUnitByUuidResponse createGetUnitByUuidResponse() {
        return new GetUnitByUuidResponse();
    }

    /**
     * Create an instance of {@link GetResearchMethodChangesListResponse }
     * 
     */
    public GetResearchMethodChangesListResponse createGetResearchMethodChangesListResponse() {
        return new GetResearchMethodChangesListResponse();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link IncorporationForm }
     * 
     */
    public IncorporationForm createIncorporationForm() {
        return new IncorporationForm();
    }

    /**
     * Create an instance of {@link RegionalizationRequirement }
     * 
     */
    public RegionalizationRequirement createRegionalizationRequirement() {
        return new RegionalizationRequirement();
    }

    /**
     * Create an instance of {@link ProductMarks }
     * 
     */
    public ProductMarks createProductMarks() {
        return new ProductMarks();
    }

    /**
     * Create an instance of {@link ProductItemProducing }
     * 
     */
    public ProductItemProducing createProductItemProducing() {
        return new ProductItemProducing();
    }

    /**
     * Create an instance of {@link Street }
     * 
     */
    public Street createStreet() {
        return new Street();
    }

    /**
     * Create an instance of {@link Locality }
     * 
     */
    public Locality createLocality() {
        return new Locality();
    }

    /**
     * Create an instance of {@link EnterpriseActivity }
     * 
     */
    public EnterpriseActivity createEnterpriseActivity() {
        return new EnterpriseActivity();
    }

    /**
     * Create an instance of {@link PackingType }
     * 
     */
    public PackingType createPackingType() {
        return new PackingType();
    }

    /**
     * Create an instance of {@link Producer }
     * 
     */
    public Producer createProducer() {
        return new Producer();
    }

    /**
     * Create an instance of {@link EnterpriseNumberList }
     * 
     */
    public EnterpriseNumberList createEnterpriseNumberList() {
        return new EnterpriseNumberList();
    }

    /**
     * Create an instance of {@link Packaging }
     * 
     */
    public Packaging createPackaging() {
        return new Packaging();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link RegionalizationRegionStatus }
     * 
     */
    public RegionalizationRegionStatus createRegionalizationRegionStatus() {
        return new RegionalizationRegionStatus();
    }

    /**
     * Create an instance of {@link RegionalizationCondition }
     * 
     */
    public RegionalizationCondition createRegionalizationCondition() {
        return new RegionalizationCondition();
    }

    /**
     * Create an instance of {@link AddressObjectView }
     * 
     */
    public AddressObjectView createAddressObjectView() {
        return new AddressObjectView();
    }

    /**
     * Create an instance of {@link Organization }
     * 
     */
    public Organization createOrganization() {
        return new Organization();
    }

    /**
     * Create an instance of {@link FederalDistrict }
     * 
     */
    public FederalDistrict createFederalDistrict() {
        return new FederalDistrict();
    }

    /**
     * Create an instance of {@link RegionalizationConditionGroup }
     * 
     */
    public RegionalizationConditionGroup createRegionalizationConditionGroup() {
        return new RegionalizationConditionGroup();
    }

    /**
     * Create an instance of {@link EnterpriseActivityList }
     * 
     */
    public EnterpriseActivityList createEnterpriseActivityList() {
        return new EnterpriseActivityList();
    }

    /**
     * Create an instance of {@link ComplexDate }
     * 
     */
    public ComplexDate createComplexDate() {
        return new ComplexDate();
    }

    /**
     * Create an instance of {@link ProducerList }
     * 
     */
    public ProducerList createProducerList() {
        return new ProducerList();
    }

    /**
     * Create an instance of {@link MedicinalDrug }
     * 
     */
    public MedicinalDrug createMedicinalDrug() {
        return new MedicinalDrug();
    }

    /**
     * Create an instance of {@link Indicator }
     * 
     */
    public Indicator createIndicator() {
        return new Indicator();
    }

    /**
     * Create an instance of {@link EnterpriseOfficialRegistration }
     * 
     */
    public EnterpriseOfficialRegistration createEnterpriseOfficialRegistration() {
        return new EnterpriseOfficialRegistration();
    }

    /**
     * Create an instance of {@link PackageList }
     * 
     */
    public PackageList createPackageList() {
        return new PackageList();
    }

    /**
     * Create an instance of {@link RegionalizationShippingRule }
     * 
     */
    public RegionalizationShippingRule createRegionalizationShippingRule() {
        return new RegionalizationShippingRule();
    }

    /**
     * Create an instance of {@link RegionalizationStatus }
     * 
     */
    public RegionalizationStatus createRegionalizationStatus() {
        return new RegionalizationStatus();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link BusinessEntity.ActivityLocation }
     * 
     */
    public BusinessEntity.ActivityLocation createBusinessEntityActivityLocation() {
        return new BusinessEntity.ActivityLocation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOptions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "listOptions")
    public JAXBElement<ListOptions> createListOptions(ListOptions value) {
        return new JAXBElement<ListOptions>(_ListOptions_QNAME, ListOptions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessEntityList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "businessEntityList")
    public JAXBElement<BusinessEntityList> createBusinessEntityList(BusinessEntityList value) {
        return new JAXBElement<BusinessEntityList>(_BusinessEntityList_QNAME, BusinessEntityList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "uuid")
    public JAXBElement<String> createUuid(String value) {
        return new JAXBElement<String>(_Uuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "districtGuid")
    public JAXBElement<String> createDistrictGuid(String value) {
        return new JAXBElement<String>(_DistrictGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "requestRejectedFault")
    public JAXBElement<FaultInfo> createRequestRejectedFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_RequestRejectedFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionalizationShippingRuleList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nShippingRuleList")
    public JAXBElement<RegionalizationShippingRuleList> createR13NShippingRuleList(RegionalizationShippingRuleList value) {
        return new JAXBElement<RegionalizationShippingRuleList>(_R13NShippingRuleList_QNAME, RegionalizationShippingRuleList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "internalServiceFault")
    public JAXBElement<FaultInfo> createInternalServiceFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_InternalServiceFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "incorrectRequestFault")
    public JAXBElement<FaultInfo> createIncorrectRequestFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_IncorrectRequestFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "regionList")
    public JAXBElement<RegionList> createRegionList(RegionList value) {
        return new JAXBElement<RegionList>(_RegionList_QNAME, RegionList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionalizationConditionList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nConditionList")
    public JAXBElement<RegionalizationConditionList> createR13NConditionList(RegionalizationConditionList value) {
        return new JAXBElement<RegionalizationConditionList>(_R13NConditionList_QNAME, RegionalizationConditionList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Product }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "product")
    public JAXBElement<Product> createProduct(Product value) {
        return new JAXBElement<Product>(_Product_QNAME, Product.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnimalDisease }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "disease")
    public JAXBElement<AnimalDisease> createDisease(AnimalDisease value) {
        return new JAXBElement<AnimalDisease>(_Disease_QNAME, AnimalDisease.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productType")
    public JAXBElement<BigInteger> createProductType(BigInteger value) {
        return new JAXBElement<BigInteger>(_ProductType_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionalizationRegionStatusList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nRegionStatusList")
    public JAXBElement<RegionalizationRegionStatusList> createR13NRegionStatusList(RegionalizationRegionStatusList value) {
        return new JAXBElement<RegionalizationRegionStatusList>(_R13NRegionStatusList_QNAME, RegionalizationRegionStatusList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResearchMethodList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "researchMethodList")
    public JAXBElement<ResearchMethodList> createResearchMethodList(ResearchMethodList value) {
        return new JAXBElement<ResearchMethodList>(_ResearchMethodList_QNAME, ResearchMethodList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "businessMember")
    public JAXBElement<BusinessMember> createBusinessMember(BusinessMember value) {
        return new JAXBElement<BusinessMember>(_BusinessMember_QNAME, BusinessMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityLocationList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "activityLocationList")
    public JAXBElement<ActivityLocationList> createActivityLocationList(ActivityLocationList value) {
        return new JAXBElement<ActivityLocationList>(_ActivityLocationList_QNAME, ActivityLocationList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnterpriseList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterpriseList")
    public JAXBElement<EnterpriseList> createEnterpriseList(EnterpriseList value) {
        return new JAXBElement<EnterpriseList>(_EnterpriseList_QNAME, EnterpriseList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "subProductList")
    public JAXBElement<SubProductList> createSubProductList(SubProductList value) {
        return new JAXBElement<SubProductList>(_SubProductList_QNAME, SubProductList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResearchMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "researchMethod")
    public JAXBElement<ResearchMethod> createResearchMethod(ResearchMethod value) {
        return new JAXBElement<ResearchMethod>(_ResearchMethod_QNAME, ResearchMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "businessEntity")
    public JAXBElement<BusinessEntity> createBusinessEntity(BusinessEntity value) {
        return new JAXBElement<BusinessEntity>(_BusinessEntity_QNAME, BusinessEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnitList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "unitList")
    public JAXBElement<UnitList> createUnitList(UnitList value) {
        return new JAXBElement<UnitList>(_UnitList_QNAME, UnitList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "regionGuid")
    public JAXBElement<String> createRegionGuid(String value) {
        return new JAXBElement<String>(_RegionGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "countryGuid")
    public JAXBElement<String> createCountryGuid(String value) {
        return new JAXBElement<String>(_CountryGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "globalID")
    public JAXBElement<String> createGlobalID(String value) {
        return new JAXBElement<String>(_GlobalID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Enterprise }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterprise")
    public JAXBElement<Enterprise> createEnterprise(Enterprise value) {
        return new JAXBElement<Enterprise>(_Enterprise_QNAME, Enterprise.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterpriseGuid")
    public JAXBElement<String> createEnterpriseGuid(String value) {
        return new JAXBElement<String>(_EnterpriseGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateInterval }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "updateDateInterval")
    public JAXBElement<DateInterval> createUpdateDateInterval(DateInterval value) {
        return new JAXBElement<DateInterval>(_UpdateDateInterval_QNAME, DateInterval.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Purpose }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "purpose")
    public JAXBElement<Purpose> createPurpose(Purpose value) {
        return new JAXBElement<Purpose>(_Purpose_QNAME, Purpose.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "cargoType")
    public JAXBElement<SubProduct> createCargoType(SubProduct value) {
        return new JAXBElement<SubProduct>(_CargoType_QNAME, SubProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productList")
    public JAXBElement<ProductList> createProductList(ProductList value) {
        return new JAXBElement<ProductList>(_ProductList_QNAME, ProductList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Region }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "region")
    public JAXBElement<Region> createRegion(Region value) {
        return new JAXBElement<Region>(_Region_QNAME, Region.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurposeList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "purposeList")
    public JAXBElement<PurposeList> createPurposeList(PurposeList value) {
        return new JAXBElement<PurposeList>(_PurposeList_QNAME, PurposeList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "accessDeniedFault")
    public JAXBElement<FaultInfo> createAccessDeniedFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_AccessDeniedFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalityList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "localityList")
    public JAXBElement<LocalityList> createLocalityList(LocalityList value) {
        return new JAXBElement<LocalityList>(_LocalityList_QNAME, LocalityList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StreetList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "streetList")
    public JAXBElement<StreetList> createStreetList(StreetList value) {
        return new JAXBElement<StreetList>(_StreetList_QNAME, StreetList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "subProductGuid")
    public JAXBElement<String> createSubProductGuid(String value) {
        return new JAXBElement<String>(_SubProductGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link District }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "district")
    public JAXBElement<District> createDistrict(District value) {
        return new JAXBElement<District>(_District_QNAME, District.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Unit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "unit")
    public JAXBElement<Unit> createUnit(Unit value) {
        return new JAXBElement<Unit>(_Unit_QNAME, Unit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "offsetOutOfRangeFault")
    public JAXBElement<FaultInfo> createOffsetOutOfRangeFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_OffsetOutOfRangeFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productItem")
    public JAXBElement<ProductItem> createProductItem(ProductItem value) {
        return new JAXBElement<ProductItem>(_ProductItem_QNAME, ProductItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "subProduct")
    public JAXBElement<SubProduct> createSubProduct(SubProduct value) {
        return new JAXBElement<SubProduct>(_SubProduct_QNAME, SubProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Area }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nZone")
    public JAXBElement<Area> createR13NZone(Area value) {
        return new JAXBElement<Area>(_R13NZone_QNAME, Area.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountryList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "countryList")
    public JAXBElement<CountryList> createCountryList(CountryList value) {
        return new JAXBElement<CountryList>(_CountryList_QNAME, CountryList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "entityNotFoundFault")
    public JAXBElement<FaultInfo> createEntityNotFoundFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_EntityNotFoundFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterpriseGroup")
    public JAXBElement<BigInteger> createEnterpriseGroup(BigInteger value) {
        return new JAXBElement<BigInteger>(_EnterpriseGroup_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "localityGuid")
    public JAXBElement<String> createLocalityGuid(String value) {
        return new JAXBElement<String>(_LocalityGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DistrictList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "districtList")
    public JAXBElement<DistrictList> createDistrictList(DistrictList value) {
        return new JAXBElement<DistrictList>(_DistrictList_QNAME, DistrictList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnimalDiseaseList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "diseaseList")
    public JAXBElement<AnimalDiseaseList> createDiseaseList(AnimalDiseaseList value) {
        return new JAXBElement<AnimalDiseaseList>(_DiseaseList_QNAME, AnimalDiseaseList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductItemList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productItemList")
    public JAXBElement<ProductItemList> createProductItemList(ProductItemList value) {
        return new JAXBElement<ProductItemList>(_ProductItemList_QNAME, ProductItemList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productGuid")
    public JAXBElement<String> createProductGuid(String value) {
        return new JAXBElement<String>(_ProductGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Country }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "country")
    public JAXBElement<Country> createCountry(Country value) {
        return new JAXBElement<Country>(_Country_QNAME, Country.class, null, value);
    }

}
