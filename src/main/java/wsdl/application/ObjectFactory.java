
package wsdl.application;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This application contains factory methods for each
 * Java content interface and Java element interface 
 * generated in the wsdl.application package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOptions_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "listOptions");
    private final static QName _GetStockEntryByUuidResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryByUuidResponse");
    private final static QName _UpdateTransportMovementDetailsResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateTransportMovementDetailsResponse");
    private final static QName _GetAppliedUserAuthorityListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getAppliedUserAuthorityListResponse");
    private final static QName _GetStockEntryByGuidResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryByGuidResponse");
    private final static QName _BusinessEntityList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "businessEntityList");
    private final static QName _RequestRejectedFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "requestRejectedFault");
    private final static QName _StockEntry_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "stockEntry");
    private final static QName _R13NShippingRuleList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nShippingRuleList");
    private final static QName _StockEntryList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "stockEntryList");
    private final static QName _GetStockEntryListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryListResponse");
    private final static QName _RegionList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "regionList");
    private final static QName _Disease_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "disease");
    private final static QName _ProductType_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productType");
    private final static QName _ModifyEnterpriseRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyEnterpriseRequest");
    private final static QName _ResearchMethodList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "researchMethodList");
    private final static QName _BusinessMember_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "businessMember");
    private final static QName _RegisterProductionOperationResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "registerProductionOperationResponse");
    private final static QName _SubProductList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "subProductList");
    private final static QName _ResearchMethod_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "researchMethod");
    private final static QName _UnitList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "unitList");
    private final static QName _CountryGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "countryGuid");
    private final static QName _RegionGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "regionGuid");
    private final static QName _Application_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application", "application");
    private final static QName _UpdateUserWorkingAreasResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateUserWorkingAreasResponse");
    private final static QName _ProcessIncomingConsignmentResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "processIncomingConsignmentResponse");
    private final static QName _ModifyActivityLocationsResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyActivityLocationsResponse");
    private final static QName _Purpose_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "purpose");
    private final static QName _GetVetDocumentChangesListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getVetDocumentChangesListRequest");
    private final static QName _PurposeList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "purposeList");
    private final static QName _Region_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "region");
    private final static QName _ModifyBusinessEntityRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyBusinessEntityRequest");
    private final static QName _StreetList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "streetList");
    private final static QName _PrepareOutgoingConsignmentResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "prepareOutgoingConsignmentResponse");
    private final static QName _UnbindBusinessEntityUserResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "unbindBusinessEntityUserResponse");
    private final static QName _SubProductGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "subProductGuid");
    private final static QName _AuthorityList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "authorityList");
    private final static QName _District_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "district");
    private final static QName _Unit_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "unit");
    private final static QName _GetBusinessEntityUserRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getBusinessEntityUserRequest");
    private final static QName _ProductItem_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productItem");
    private final static QName _SubProduct_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "subProduct");
    private final static QName _MergeStockEntriesRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "mergeStockEntriesRequest");
    private final static QName _CountryList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "countryList");
    private final static QName _ApplicationResultData_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application", "applicationResultData");
    private final static QName _EntityNotFoundFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "entityNotFoundFault");
    private final static QName _User_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "user");
    private final static QName _UnbindBusinessEntityUserRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "unbindBusinessEntityUserRequest");
    private final static QName _ModifyEnterpriseResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyEnterpriseResponse");
    private final static QName _EnterpriseGroup_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterpriseGroup");
    private final static QName _ResolveDiscrepancyRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "resolveDiscrepancyRequest");
    private final static QName _UpdateUserAuthoritiesRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateUserAuthoritiesRequest");
    private final static QName _GetVetDocumentListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getVetDocumentListResponse");
    private final static QName _UpdateVeterinaryEventsResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateVeterinaryEventsResponse");
    private final static QName _VetDocumentType_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "vetDocumentType");
    private final static QName _R13NRouteSection_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "r13nRouteSection");
    private final static QName _GetBusinessEntityUserResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getBusinessEntityUserResponse");
    private final static QName _ProductItemList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productItemList");
    private final static QName _UpdateUserWorkingAreasRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateUserWorkingAreasRequest");
    private final static QName _ApplicationData_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application", "applicationData");
    private final static QName _StockEntryUuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "stockEntryUuid");
    private final static QName _RegisterProductionOperationRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "registerProductionOperationRequest");
    private final static QName _Country_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "country");
    private final static QName _GetStockEntryVersionListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryVersionListRequest");
    private final static QName _DeliveryParticipant_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "deliveryParticipant");
    private final static QName _UpdateVeterinaryEventsRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateVeterinaryEventsRequest");
    private final static QName _Uuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "uuid");
    private final static QName _AddBusinessEntityUserRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "addBusinessEntityUserRequest");
    private final static QName _GetBusinessEntityUserListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getBusinessEntityUserListResponse");
    private final static QName _UpdateUserAuthoritiesResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateUserAuthoritiesResponse");
    private final static QName _DistrictGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "districtGuid");
    private final static QName _UnsupportedApplicationDataTypeFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application/ws-definitions", "unsupportedApplicationDataTypeFault");
    private final static QName _GetBusinessEntityUserListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getBusinessEntityUserListRequest");
    private final static QName _InternalServiceFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "internalServiceFault");
    private final static QName _WithdrawVetDocumentRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "withdrawVetDocumentRequest");
    private final static QName _VetDocumentStatus_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "vetDocumentStatus");
    private final static QName _IncorrectRequestFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "incorrectRequestFault");
    private final static QName _GetStockEntryChangesListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryChangesListRequest");
    private final static QName _Product_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "product");
    private final static QName _R13NConditionList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nConditionList");
    private final static QName _R13NRegionStatusList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nRegionStatusList");
    private final static QName _UnknownServiceIdFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application/ws-definitions", "unknownServiceIdFault");
    private final static QName _AddBusinessEntityUserResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "addBusinessEntityUserResponse");
    private final static QName _ErrorList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application", "errorList");
    private final static QName _ModifyBusinessEntityResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyBusinessEntityResponse");
    private final static QName _ActivityLocationList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "activityLocationList");
    private final static QName _EnterpriseList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterpriseList");
    private final static QName _BusinessEntity_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "businessEntity");
    private final static QName _GlobalID_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "globalID");
    private final static QName _Enterprise_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterprise");
    private final static QName _UserList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "userList");
    private final static QName _EnterpriseGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "enterpriseGuid");
    private final static QName _Guid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "guid");
    private final static QName _UpdateDateInterval_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base", "updateDateInterval");
    private final static QName _GetStockEntryVersionListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryVersionListResponse");
    private final static QName _MergeStockEntriesResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "mergeStockEntriesResponse");
    private final static QName _CargoType_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "cargoType");
    private final static QName _ModifyProducerStockListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyProducerStockListRequest");
    private final static QName _ProductList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productList");
    private final static QName _AccessDeniedFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "accessDeniedFault");
    private final static QName _ResolveDiscrepancyResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "resolveDiscrepancyResponse");
    private final static QName _GetVetDocumentByUuidResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getVetDocumentByUuidResponse");
    private final static QName _LocalityList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "localityList");
    private final static QName _ProcessIncomingConsignmentRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "processIncomingConsignmentRequest");
    private final static QName _UpdateTransportMovementDetailsRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "updateTransportMovementDetailsRequest");
    private final static QName _OffsetOutOfRangeFault_QNAME = new QName("http://api.vetrf.ru/schema/cdm/base/ws-definitions", "offsetOutOfRangeFault");
    private final static QName _VetDocumentList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "vetDocumentList");
    private final static QName _StockEntryEventList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "stockEntryEventList");
    private final static QName _GetStockEntryByGuidRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryByGuidRequest");
    private final static QName _R13NZone_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "r13nZone");
    private final static QName _PrepareOutgoingConsignmentRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "prepareOutgoingConsignmentRequest");
    private final static QName _VetDocument_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "vetDocument");
    private final static QName _ModifyProducerStockListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyProducerStockListResponse");
    private final static QName _GetVetDocumentByUuidRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getVetDocumentByUuidRequest");
    private final static QName _CheckShipmentRegionalizationResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "checkShipmentRegionalizationResponse");
    private final static QName _GetVetDocumentListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getVetDocumentListRequest");
    private final static QName _ShipmentRoute_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "shipmentRoute");
    private final static QName _LocalityGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "localityGuid");
    private final static QName _GetStockEntryListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryListRequest");
    private final static QName _GetStockEntryChangesListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryChangesListResponse");
    private final static QName _ModifyActivityLocationsRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "modifyActivityLocationsRequest");
    private final static QName _DistrictList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "districtList");
    private final static QName _VetDocumentUuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", "vetDocumentUuid");
    private final static QName _DiseaseList_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "diseaseList");
    private final static QName _GetAppliedUserAuthorityListRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getAppliedUserAuthorityListRequest");
    private final static QName _ProductGuid_QNAME = new QName("http://api.vetrf.ru/schema/cdm/dictionary/v2", "productGuid");
    private final static QName _WithdrawVetDocumentResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "withdrawVetDocumentResponse");
    private final static QName _CheckShipmentRegionalizationRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "checkShipmentRegionalizationRequest");
    private final static QName _BusinessError_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application", "businessError");
    private final static QName _GetStockEntryByUuidRequest_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getStockEntryByUuidRequest");
    private final static QName _Binary_QNAME = new QName("http://api.vetrf.ru/schema/cdm/application", "binary");
    private final static QName _GetVetDocumentChangesListResponse_QNAME = new QName("http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", "getVetDocumentChangesListResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsdl.application
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BEActivityLocationsModificationOperation }
     * 
     */
    public BEActivityLocationsModificationOperation createBEActivityLocationsModificationOperation() {
        return new BEActivityLocationsModificationOperation();
    }

    /**
     * Create an instance of {@link BusinessEntity }
     * 
     */
    public BusinessEntity createBusinessEntity() {
        return new BusinessEntity();
    }

    /**
     * Create an instance of {@link ReceiveApplicationResultRequest }
     * 
     */
    public ReceiveApplicationResultRequest createReceiveApplicationResultRequest() {
        return new ReceiveApplicationResultRequest();
    }

    /**
     * Create an instance of {@link SubmitApplicationResponse }
     * 
     */
    public SubmitApplicationResponse createSubmitApplicationResponse() {
        return new SubmitApplicationResponse();
    }

    /**
     * Create an instance of {@link Application }
     * 
     */
    public Application createApplication() {
        return new Application();
    }

    /**
     * Create an instance of {@link ReceiveApplicationResultResponse }
     * 
     */
    public ReceiveApplicationResultResponse createReceiveApplicationResultResponse() {
        return new ReceiveApplicationResultResponse();
    }

    /**
     * Create an instance of {@link FaultInfo }
     * 
     */
    public FaultInfo createFaultInfo() {
        return new FaultInfo();
    }

    /**
     * Create an instance of {@link SubmitApplicationRequest }
     * 
     */
    public SubmitApplicationRequest createSubmitApplicationRequest() {
        return new SubmitApplicationRequest();
    }

    /**
     * Create an instance of {@link ListOptions }
     * 
     */
    public ListOptions createListOptions() {
        return new ListOptions();
    }

    /**
     * Create an instance of {@link DateInterval }
     * 
     */
    public DateInterval createDateInterval() {
        return new DateInterval();
    }

    /**
     * Create an instance of {@link EntityList }
     * 
     */
    public EntityList createEntityList() {
        return new EntityList();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link GenericEntity }
     * 
     */
    public GenericEntity createGenericEntity() {
        return new GenericEntity();
    }

    /**
     * Create an instance of {@link GenericVersioningEntity }
     * 
     */
    public GenericVersioningEntity createGenericVersioningEntity() {
        return new GenericVersioningEntity();
    }

    /**
     * Create an instance of {@link BusinessError }
     * 
     */
    public BusinessError createBusinessError() {
        return new BusinessError();
    }

    /**
     * Create an instance of {@link BusinessErrorList }
     * 
     */
    public BusinessErrorList createBusinessErrorList() {
        return new BusinessErrorList();
    }

    /**
     * Create an instance of {@link ApplicationDataWrapper }
     * 
     */
    public ApplicationDataWrapper createApplicationDataWrapper() {
        return new ApplicationDataWrapper();
    }

    /**
     * Create an instance of {@link ApplicationResultWrapper }
     * 
     */
    public ApplicationResultWrapper createApplicationResultWrapper() {
        return new ApplicationResultWrapper();
    }

    /**
     * Create an instance of {@link UpdateVeterinaryEventsResponse }
     * 
     */
    public UpdateVeterinaryEventsResponse createUpdateVeterinaryEventsResponse() {
        return new UpdateVeterinaryEventsResponse();
    }

    /**
     * Create an instance of {@link ModifyActivityLocationsRequest }
     * 
     */
    public ModifyActivityLocationsRequest createModifyActivityLocationsRequest() {
        return new ModifyActivityLocationsRequest();
    }

    /**
     * Create an instance of {@link GetVetDocumentListResponse }
     * 
     */
    public GetVetDocumentListResponse createGetVetDocumentListResponse() {
        return new GetVetDocumentListResponse();
    }

    /**
     * Create an instance of {@link GetStockEntryListRequest }
     * 
     */
    public GetStockEntryListRequest createGetStockEntryListRequest() {
        return new GetStockEntryListRequest();
    }

    /**
     * Create an instance of {@link GetStockEntryChangesListResponse }
     * 
     */
    public GetStockEntryChangesListResponse createGetStockEntryChangesListResponse() {
        return new GetStockEntryChangesListResponse();
    }

    /**
     * Create an instance of {@link UpdateUserAuthoritiesRequest }
     * 
     */
    public UpdateUserAuthoritiesRequest createUpdateUserAuthoritiesRequest() {
        return new UpdateUserAuthoritiesRequest();
    }

    /**
     * Create an instance of {@link ResolveDiscrepancyRequest }
     * 
     */
    public ResolveDiscrepancyRequest createResolveDiscrepancyRequest() {
        return new ResolveDiscrepancyRequest();
    }

    /**
     * Create an instance of {@link GetVetDocumentListRequest }
     * 
     */
    public GetVetDocumentListRequest createGetVetDocumentListRequest() {
        return new GetVetDocumentListRequest();
    }

    /**
     * Create an instance of {@link CheckShipmentRegionalizationResponse }
     * 
     */
    public CheckShipmentRegionalizationResponse createCheckShipmentRegionalizationResponse() {
        return new CheckShipmentRegionalizationResponse();
    }

    /**
     * Create an instance of {@link GetVetDocumentChangesListResponse }
     * 
     */
    public GetVetDocumentChangesListResponse createGetVetDocumentChangesListResponse() {
        return new GetVetDocumentChangesListResponse();
    }

    /**
     * Create an instance of {@link RegisterProductionOperationRequest }
     * 
     */
    public RegisterProductionOperationRequest createRegisterProductionOperationRequest() {
        return new RegisterProductionOperationRequest();
    }

    /**
     * Create an instance of {@link GetStockEntryByUuidRequest }
     * 
     */
    public GetStockEntryByUuidRequest createGetStockEntryByUuidRequest() {
        return new GetStockEntryByUuidRequest();
    }

    /**
     * Create an instance of {@link CheckShipmentRegionalizationRequest }
     * 
     */
    public CheckShipmentRegionalizationRequest createCheckShipmentRegionalizationRequest() {
        return new CheckShipmentRegionalizationRequest();
    }

    /**
     * Create an instance of {@link WithdrawVetDocumentResponse }
     * 
     */
    public WithdrawVetDocumentResponse createWithdrawVetDocumentResponse() {
        return new WithdrawVetDocumentResponse();
    }

    /**
     * Create an instance of {@link UpdateUserWorkingAreasRequest }
     * 
     */
    public UpdateUserWorkingAreasRequest createUpdateUserWorkingAreasRequest() {
        return new UpdateUserWorkingAreasRequest();
    }

    /**
     * Create an instance of {@link GetAppliedUserAuthorityListRequest }
     * 
     */
    public GetAppliedUserAuthorityListRequest createGetAppliedUserAuthorityListRequest() {
        return new GetAppliedUserAuthorityListRequest();
    }

    /**
     * Create an instance of {@link GetBusinessEntityUserResponse }
     * 
     */
    public GetBusinessEntityUserResponse createGetBusinessEntityUserResponse() {
        return new GetBusinessEntityUserResponse();
    }

    /**
     * Create an instance of {@link GetStockEntryByGuidRequest }
     * 
     */
    public GetStockEntryByGuidRequest createGetStockEntryByGuidRequest() {
        return new GetStockEntryByGuidRequest();
    }

    /**
     * Create an instance of {@link GetBusinessEntityUserRequest }
     * 
     */
    public GetBusinessEntityUserRequest createGetBusinessEntityUserRequest() {
        return new GetBusinessEntityUserRequest();
    }

    /**
     * Create an instance of {@link UpdateTransportMovementDetailsRequest }
     * 
     */
    public UpdateTransportMovementDetailsRequest createUpdateTransportMovementDetailsRequest() {
        return new UpdateTransportMovementDetailsRequest();
    }

    /**
     * Create an instance of {@link ProcessIncomingConsignmentRequest }
     * 
     */
    public ProcessIncomingConsignmentRequest createProcessIncomingConsignmentRequest() {
        return new ProcessIncomingConsignmentRequest();
    }

    /**
     * Create an instance of {@link PrepareOutgoingConsignmentResponse }
     * 
     */
    public PrepareOutgoingConsignmentResponse createPrepareOutgoingConsignmentResponse() {
        return new PrepareOutgoingConsignmentResponse();
    }

    /**
     * Create an instance of {@link UnbindBusinessEntityUserResponse }
     * 
     */
    public UnbindBusinessEntityUserResponse createUnbindBusinessEntityUserResponse() {
        return new UnbindBusinessEntityUserResponse();
    }

    /**
     * Create an instance of {@link ModifyBusinessEntityRequest }
     * 
     */
    public ModifyBusinessEntityRequest createModifyBusinessEntityRequest() {
        return new ModifyBusinessEntityRequest();
    }

    /**
     * Create an instance of {@link ResolveDiscrepancyResponse }
     * 
     */
    public ResolveDiscrepancyResponse createResolveDiscrepancyResponse() {
        return new ResolveDiscrepancyResponse();
    }

    /**
     * Create an instance of {@link GetVetDocumentByUuidResponse }
     * 
     */
    public GetVetDocumentByUuidResponse createGetVetDocumentByUuidResponse() {
        return new GetVetDocumentByUuidResponse();
    }

    /**
     * Create an instance of {@link ModifyEnterpriseResponse }
     * 
     */
    public ModifyEnterpriseResponse createModifyEnterpriseResponse() {
        return new ModifyEnterpriseResponse();
    }

    /**
     * Create an instance of {@link ModifyProducerStockListResponse }
     * 
     */
    public ModifyProducerStockListResponse createModifyProducerStockListResponse() {
        return new ModifyProducerStockListResponse();
    }

    /**
     * Create an instance of {@link GetVetDocumentByUuidRequest }
     * 
     */
    public GetVetDocumentByUuidRequest createGetVetDocumentByUuidRequest() {
        return new GetVetDocumentByUuidRequest();
    }

    /**
     * Create an instance of {@link UnbindBusinessEntityUserRequest }
     * 
     */
    public UnbindBusinessEntityUserRequest createUnbindBusinessEntityUserRequest() {
        return new UnbindBusinessEntityUserRequest();
    }

    /**
     * Create an instance of {@link PrepareOutgoingConsignmentRequest }
     * 
     */
    public PrepareOutgoingConsignmentRequest createPrepareOutgoingConsignmentRequest() {
        return new PrepareOutgoingConsignmentRequest();
    }

    /**
     * Create an instance of {@link MergeStockEntriesRequest }
     * 
     */
    public MergeStockEntriesRequest createMergeStockEntriesRequest() {
        return new MergeStockEntriesRequest();
    }

    /**
     * Create an instance of {@link ModifyBusinessEntityResponse }
     * 
     */
    public ModifyBusinessEntityResponse createModifyBusinessEntityResponse() {
        return new ModifyBusinessEntityResponse();
    }

    /**
     * Create an instance of {@link RegisterProductionOperationResponse }
     * 
     */
    public RegisterProductionOperationResponse createRegisterProductionOperationResponse() {
        return new RegisterProductionOperationResponse();
    }

    /**
     * Create an instance of {@link AddBusinessEntityUserResponse }
     * 
     */
    public AddBusinessEntityUserResponse createAddBusinessEntityUserResponse() {
        return new AddBusinessEntityUserResponse();
    }

    /**
     * Create an instance of {@link ModifyEnterpriseRequest }
     * 
     */
    public ModifyEnterpriseRequest createModifyEnterpriseRequest() {
        return new ModifyEnterpriseRequest();
    }

    /**
     * Create an instance of {@link ModifyProducerStockListRequest }
     * 
     */
    public ModifyProducerStockListRequest createModifyProducerStockListRequest() {
        return new ModifyProducerStockListRequest();
    }

    /**
     * Create an instance of {@link MergeStockEntriesResponse }
     * 
     */
    public MergeStockEntriesResponse createMergeStockEntriesResponse() {
        return new MergeStockEntriesResponse();
    }

    /**
     * Create an instance of {@link GetVetDocumentChangesListRequest }
     * 
     */
    public GetVetDocumentChangesListRequest createGetVetDocumentChangesListRequest() {
        return new GetVetDocumentChangesListRequest();
    }

    /**
     * Create an instance of {@link GetStockEntryVersionListResponse }
     * 
     */
    public GetStockEntryVersionListResponse createGetStockEntryVersionListResponse() {
        return new GetStockEntryVersionListResponse();
    }

    /**
     * Create an instance of {@link ProcessIncomingConsignmentResponse }
     * 
     */
    public ProcessIncomingConsignmentResponse createProcessIncomingConsignmentResponse() {
        return new ProcessIncomingConsignmentResponse();
    }

    /**
     * Create an instance of {@link ModifyActivityLocationsResponse }
     * 
     */
    public ModifyActivityLocationsResponse createModifyActivityLocationsResponse() {
        return new ModifyActivityLocationsResponse();
    }

    /**
     * Create an instance of {@link UpdateUserWorkingAreasResponse }
     * 
     */
    public UpdateUserWorkingAreasResponse createUpdateUserWorkingAreasResponse() {
        return new UpdateUserWorkingAreasResponse();
    }

    /**
     * Create an instance of {@link UpdateVeterinaryEventsRequest }
     * 
     */
    public UpdateVeterinaryEventsRequest createUpdateVeterinaryEventsRequest() {
        return new UpdateVeterinaryEventsRequest();
    }

    /**
     * Create an instance of {@link GetStockEntryByGuidResponse }
     * 
     */
    public GetStockEntryByGuidResponse createGetStockEntryByGuidResponse() {
        return new GetStockEntryByGuidResponse();
    }

    /**
     * Create an instance of {@link GetAppliedUserAuthorityListResponse }
     * 
     */
    public GetAppliedUserAuthorityListResponse createGetAppliedUserAuthorityListResponse() {
        return new GetAppliedUserAuthorityListResponse();
    }

    /**
     * Create an instance of {@link UpdateTransportMovementDetailsResponse }
     * 
     */
    public UpdateTransportMovementDetailsResponse createUpdateTransportMovementDetailsResponse() {
        return new UpdateTransportMovementDetailsResponse();
    }

    /**
     * Create an instance of {@link GetStockEntryByUuidResponse }
     * 
     */
    public GetStockEntryByUuidResponse createGetStockEntryByUuidResponse() {
        return new GetStockEntryByUuidResponse();
    }

    /**
     * Create an instance of {@link GetStockEntryVersionListRequest }
     * 
     */
    public GetStockEntryVersionListRequest createGetStockEntryVersionListRequest() {
        return new GetStockEntryVersionListRequest();
    }

    /**
     * Create an instance of {@link GetStockEntryChangesListRequest }
     * 
     */
    public GetStockEntryChangesListRequest createGetStockEntryChangesListRequest() {
        return new GetStockEntryChangesListRequest();
    }

    /**
     * Create an instance of {@link WithdrawVetDocumentRequest }
     * 
     */
    public WithdrawVetDocumentRequest createWithdrawVetDocumentRequest() {
        return new WithdrawVetDocumentRequest();
    }

    /**
     * Create an instance of {@link GetBusinessEntityUserListRequest }
     * 
     */
    public GetBusinessEntityUserListRequest createGetBusinessEntityUserListRequest() {
        return new GetBusinessEntityUserListRequest();
    }

    /**
     * Create an instance of {@link GetStockEntryListResponse }
     * 
     */
    public GetStockEntryListResponse createGetStockEntryListResponse() {
        return new GetStockEntryListResponse();
    }

    /**
     * Create an instance of {@link UpdateUserAuthoritiesResponse }
     * 
     */
    public UpdateUserAuthoritiesResponse createUpdateUserAuthoritiesResponse() {
        return new UpdateUserAuthoritiesResponse();
    }

    /**
     * Create an instance of {@link GetBusinessEntityUserListResponse }
     * 
     */
    public GetBusinessEntityUserListResponse createGetBusinessEntityUserListResponse() {
        return new GetBusinessEntityUserListResponse();
    }

    /**
     * Create an instance of {@link AddBusinessEntityUserRequest }
     * 
     */
    public AddBusinessEntityUserRequest createAddBusinessEntityUserRequest() {
        return new AddBusinessEntityUserRequest();
    }

    /**
     * Create an instance of {@link MercuryApplicationRequest }
     * 
     */
    public MercuryApplicationRequest createMercuryApplicationRequest() {
        return new MercuryApplicationRequest();
    }

    /**
     * Create an instance of {@link SubProduct }
     * 
     */
    public SubProduct createSubProduct() {
        return new SubProduct();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link Purpose }
     * 
     */
    public Purpose createPurpose() {
        return new Purpose();
    }

    /**
     * Create an instance of {@link Enterprise }
     * 
     */
    public Enterprise createEnterprise() {
        return new Enterprise();
    }

    /**
     * Create an instance of {@link UnitList }
     * 
     */
    public UnitList createUnitList() {
        return new UnitList();
    }

    /**
     * Create an instance of {@link ProductItemList }
     * 
     */
    public ProductItemList createProductItemList() {
        return new ProductItemList();
    }

    /**
     * Create an instance of {@link AnimalDiseaseList }
     * 
     */
    public AnimalDiseaseList createAnimalDiseaseList() {
        return new AnimalDiseaseList();
    }

    /**
     * Create an instance of {@link ResearchMethod }
     * 
     */
    public ResearchMethod createResearchMethod() {
        return new ResearchMethod();
    }

    /**
     * Create an instance of {@link EnterpriseList }
     * 
     */
    public EnterpriseList createEnterpriseList() {
        return new EnterpriseList();
    }

    /**
     * Create an instance of {@link SubProductList }
     * 
     */
    public SubProductList createSubProductList() {
        return new SubProductList();
    }

    /**
     * Create an instance of {@link DistrictList }
     * 
     */
    public DistrictList createDistrictList() {
        return new DistrictList();
    }

    /**
     * Create an instance of {@link ActivityLocationList }
     * 
     */
    public ActivityLocationList createActivityLocationList() {
        return new ActivityLocationList();
    }

    /**
     * Create an instance of {@link BusinessMember }
     * 
     */
    public BusinessMember createBusinessMember() {
        return new BusinessMember();
    }

    /**
     * Create an instance of {@link ResearchMethodList }
     * 
     */
    public ResearchMethodList createResearchMethodList() {
        return new ResearchMethodList();
    }

    /**
     * Create an instance of {@link RegionalizationRegionStatusList }
     * 
     */
    public RegionalizationRegionStatusList createRegionalizationRegionStatusList() {
        return new RegionalizationRegionStatusList();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link RegionalizationConditionList }
     * 
     */
    public RegionalizationConditionList createRegionalizationConditionList() {
        return new RegionalizationConditionList();
    }

    /**
     * Create an instance of {@link AnimalDisease }
     * 
     */
    public AnimalDisease createAnimalDisease() {
        return new AnimalDisease();
    }

    /**
     * Create an instance of {@link RegionList }
     * 
     */
    public RegionList createRegionList() {
        return new RegionList();
    }

    /**
     * Create an instance of {@link RegionalizationShippingRuleList }
     * 
     */
    public RegionalizationShippingRuleList createRegionalizationShippingRuleList() {
        return new RegionalizationShippingRuleList();
    }

    /**
     * Create an instance of {@link CountryList }
     * 
     */
    public CountryList createCountryList() {
        return new CountryList();
    }

    /**
     * Create an instance of {@link BusinessEntityList }
     * 
     */
    public BusinessEntityList createBusinessEntityList() {
        return new BusinessEntityList();
    }

    /**
     * Create an instance of {@link ProductItem }
     * 
     */
    public ProductItem createProductItem() {
        return new ProductItem();
    }

    /**
     * Create an instance of {@link Area }
     * 
     */
    public Area createArea() {
        return new Area();
    }

    /**
     * Create an instance of {@link Unit }
     * 
     */
    public Unit createUnit() {
        return new Unit();
    }

    /**
     * Create an instance of {@link District }
     * 
     */
    public District createDistrict() {
        return new District();
    }

    /**
     * Create an instance of {@link StreetList }
     * 
     */
    public StreetList createStreetList() {
        return new StreetList();
    }

    /**
     * Create an instance of {@link PurposeList }
     * 
     */
    public PurposeList createPurposeList() {
        return new PurposeList();
    }

    /**
     * Create an instance of {@link Region }
     * 
     */
    public Region createRegion() {
        return new Region();
    }

    /**
     * Create an instance of {@link LocalityList }
     * 
     */
    public LocalityList createLocalityList() {
        return new LocalityList();
    }

    /**
     * Create an instance of {@link ProductList }
     * 
     */
    public ProductList createProductList() {
        return new ProductList();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link IncorporationForm }
     * 
     */
    public IncorporationForm createIncorporationForm() {
        return new IncorporationForm();
    }

    /**
     * Create an instance of {@link RegionalizationRequirement }
     * 
     */
    public RegionalizationRequirement createRegionalizationRequirement() {
        return new RegionalizationRequirement();
    }

    /**
     * Create an instance of {@link ProductMarks }
     * 
     */
    public ProductMarks createProductMarks() {
        return new ProductMarks();
    }

    /**
     * Create an instance of {@link ProductItemProducing }
     * 
     */
    public ProductItemProducing createProductItemProducing() {
        return new ProductItemProducing();
    }

    /**
     * Create an instance of {@link Street }
     * 
     */
    public Street createStreet() {
        return new Street();
    }

    /**
     * Create an instance of {@link Locality }
     * 
     */
    public Locality createLocality() {
        return new Locality();
    }

    /**
     * Create an instance of {@link EnterpriseActivity }
     * 
     */
    public EnterpriseActivity createEnterpriseActivity() {
        return new EnterpriseActivity();
    }

    /**
     * Create an instance of {@link PackingType }
     * 
     */
    public PackingType createPackingType() {
        return new PackingType();
    }

    /**
     * Create an instance of {@link Producer }
     * 
     */
    public Producer createProducer() {
        return new Producer();
    }

    /**
     * Create an instance of {@link EnterpriseNumberList }
     * 
     */
    public EnterpriseNumberList createEnterpriseNumberList() {
        return new EnterpriseNumberList();
    }

    /**
     * Create an instance of {@link Packaging }
     * 
     */
    public Packaging createPackaging() {
        return new Packaging();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link RegionalizationRegionStatus }
     * 
     */
    public RegionalizationRegionStatus createRegionalizationRegionStatus() {
        return new RegionalizationRegionStatus();
    }

    /**
     * Create an instance of {@link RegionalizationCondition }
     * 
     */
    public RegionalizationCondition createRegionalizationCondition() {
        return new RegionalizationCondition();
    }

    /**
     * Create an instance of {@link AddressObjectView }
     * 
     */
    public AddressObjectView createAddressObjectView() {
        return new AddressObjectView();
    }

    /**
     * Create an instance of {@link Organization }
     * 
     */
    public Organization createOrganization() {
        return new Organization();
    }

    /**
     * Create an instance of {@link FederalDistrict }
     * 
     */
    public FederalDistrict createFederalDistrict() {
        return new FederalDistrict();
    }

    /**
     * Create an instance of {@link RegionalizationConditionGroup }
     * 
     */
    public RegionalizationConditionGroup createRegionalizationConditionGroup() {
        return new RegionalizationConditionGroup();
    }

    /**
     * Create an instance of {@link EnterpriseActivityList }
     * 
     */
    public EnterpriseActivityList createEnterpriseActivityList() {
        return new EnterpriseActivityList();
    }

    /**
     * Create an instance of {@link ComplexDate }
     * 
     */
    public ComplexDate createComplexDate() {
        return new ComplexDate();
    }

    /**
     * Create an instance of {@link ProducerList }
     * 
     */
    public ProducerList createProducerList() {
        return new ProducerList();
    }

    /**
     * Create an instance of {@link MedicinalDrug }
     * 
     */
    public MedicinalDrug createMedicinalDrug() {
        return new MedicinalDrug();
    }

    /**
     * Create an instance of {@link Indicator }
     * 
     */
    public Indicator createIndicator() {
        return new Indicator();
    }

    /**
     * Create an instance of {@link EnterpriseOfficialRegistration }
     * 
     */
    public EnterpriseOfficialRegistration createEnterpriseOfficialRegistration() {
        return new EnterpriseOfficialRegistration();
    }

    /**
     * Create an instance of {@link PackageList }
     * 
     */
    public PackageList createPackageList() {
        return new PackageList();
    }

    /**
     * Create an instance of {@link RegionalizationShippingRule }
     * 
     */
    public RegionalizationShippingRule createRegionalizationShippingRule() {
        return new RegionalizationShippingRule();
    }

    /**
     * Create an instance of {@link RegionalizationStatus }
     * 
     */
    public RegionalizationStatus createRegionalizationStatus() {
        return new RegionalizationStatus();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link VetDocumentList }
     * 
     */
    public VetDocumentList createVetDocumentList() {
        return new VetDocumentList();
    }

    /**
     * Create an instance of {@link StockEntryEventList }
     * 
     */
    public StockEntryEventList createStockEntryEventList() {
        return new StockEntryEventList();
    }

    /**
     * Create an instance of {@link RouteSectionR13NRules }
     * 
     */
    public RouteSectionR13NRules createRouteSectionR13NRules() {
        return new RouteSectionR13NRules();
    }

    /**
     * Create an instance of {@link ShipmentRoute }
     * 
     */
    public ShipmentRoute createShipmentRoute() {
        return new ShipmentRoute();
    }

    /**
     * Create an instance of {@link AuthorityList }
     * 
     */
    public AuthorityList createAuthorityList() {
        return new AuthorityList();
    }

    /**
     * Create an instance of {@link UserList }
     * 
     */
    public UserList createUserList() {
        return new UserList();
    }

    /**
     * Create an instance of {@link StockEntryList }
     * 
     */
    public StockEntryList createStockEntryList() {
        return new StockEntryList();
    }

    /**
     * Create an instance of {@link VetDocument }
     * 
     */
    public VetDocument createVetDocument() {
        return new VetDocument();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link StockEntry }
     * 
     */
    public StockEntry createStockEntry() {
        return new StockEntry();
    }

    /**
     * Create an instance of {@link DeliveryInspection }
     * 
     */
    public DeliveryInspection createDeliveryInspection() {
        return new DeliveryInspection();
    }

    /**
     * Create an instance of {@link QuarantineEvent }
     * 
     */
    public QuarantineEvent createQuarantineEvent() {
        return new QuarantineEvent();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link ProductiveBatch }
     * 
     */
    public ProductiveBatch createProductiveBatch() {
        return new ProductiveBatch();
    }

    /**
     * Create an instance of {@link GoodsDate }
     * 
     */
    public GoodsDate createGoodsDate() {
        return new GoodsDate();
    }

    /**
     * Create an instance of {@link StockEntrySearchPattern }
     * 
     */
    public StockEntrySearchPattern createStockEntrySearchPattern() {
        return new StockEntrySearchPattern();
    }

    /**
     * Create an instance of {@link TransportInfo }
     * 
     */
    public TransportInfo createTransportInfo() {
        return new TransportInfo();
    }

    /**
     * Create an instance of {@link DiscrepancyReport }
     * 
     */
    public DiscrepancyReport createDiscrepancyReport() {
        return new DiscrepancyReport();
    }

    /**
     * Create an instance of {@link WorkingArea }
     * 
     */
    public WorkingArea createWorkingArea() {
        return new WorkingArea();
    }

    /**
     * Create an instance of {@link MergeStockEntriesOperation }
     * 
     */
    public MergeStockEntriesOperation createMergeStockEntriesOperation() {
        return new MergeStockEntriesOperation();
    }

    /**
     * Create an instance of {@link RawBatch }
     * 
     */
    public RawBatch createRawBatch() {
        return new RawBatch();
    }

    /**
     * Create an instance of {@link TransportNumber }
     * 
     */
    public TransportNumber createTransportNumber() {
        return new TransportNumber();
    }

    /**
     * Create an instance of {@link LaboratoryResearchEvent }
     * 
     */
    public LaboratoryResearchEvent createLaboratoryResearchEvent() {
        return new LaboratoryResearchEvent();
    }

    /**
     * Create an instance of {@link CertifiedBatch }
     * 
     */
    public CertifiedBatch createCertifiedBatch() {
        return new CertifiedBatch();
    }

    /**
     * Create an instance of {@link ConsignmentDocumentList }
     * 
     */
    public ConsignmentDocumentList createConsignmentDocumentList() {
        return new ConsignmentDocumentList();
    }

    /**
     * Create an instance of {@link ShipmentRoutePoint }
     * 
     */
    public ShipmentRoutePoint createShipmentRoutePoint() {
        return new ShipmentRoutePoint();
    }

    /**
     * Create an instance of {@link RegionalizationClause }
     * 
     */
    public RegionalizationClause createRegionalizationClause() {
        return new RegionalizationClause();
    }

    /**
     * Create an instance of {@link UserAuthority }
     * 
     */
    public UserAuthority createUserAuthority() {
        return new UserAuthority();
    }

    /**
     * Create an instance of {@link WorkingAreaList }
     * 
     */
    public WorkingAreaList createWorkingAreaList() {
        return new WorkingAreaList();
    }

    /**
     * Create an instance of {@link VetDocumentStatusChange }
     * 
     */
    public VetDocumentStatusChange createVetDocumentStatusChange() {
        return new VetDocumentStatusChange();
    }

    /**
     * Create an instance of {@link PSLModificationOperation }
     * 
     */
    public PSLModificationOperation createPSLModificationOperation() {
        return new PSLModificationOperation();
    }

    /**
     * Create an instance of {@link ENTModificationOperation }
     * 
     */
    public ENTModificationOperation createENTModificationOperation() {
        return new ENTModificationOperation();
    }

    /**
     * Create an instance of {@link ReferencedDocument }
     * 
     */
    public ReferencedDocument createReferencedDocument() {
        return new ReferencedDocument();
    }

    /**
     * Create an instance of {@link StockDiscrepancy }
     * 
     */
    public StockDiscrepancy createStockDiscrepancy() {
        return new StockDiscrepancy();
    }

    /**
     * Create an instance of {@link Consignment }
     * 
     */
    public Consignment createConsignment() {
        return new Consignment();
    }

    /**
     * Create an instance of {@link VeterinaryEvent }
     * 
     */
    public VeterinaryEvent createVeterinaryEvent() {
        return new VeterinaryEvent();
    }

    /**
     * Create an instance of {@link ProcessingProcedure }
     * 
     */
    public ProcessingProcedure createProcessingProcedure() {
        return new ProcessingProcedure();
    }

    /**
     * Create an instance of {@link Waybill }
     * 
     */
    public Waybill createWaybill() {
        return new Waybill();
    }

    /**
     * Create an instance of {@link AnimalMedicationEvent }
     * 
     */
    public AnimalMedicationEvent createAnimalMedicationEvent() {
        return new AnimalMedicationEvent();
    }

    /**
     * Create an instance of {@link BatchOrigin }
     * 
     */
    public BatchOrigin createBatchOrigin() {
        return new BatchOrigin();
    }

    /**
     * Create an instance of {@link DeliveryFactList }
     * 
     */
    public DeliveryFactList createDeliveryFactList() {
        return new DeliveryFactList();
    }

    /**
     * Create an instance of {@link CertifiedConsignment }
     * 
     */
    public CertifiedConsignment createCertifiedConsignment() {
        return new CertifiedConsignment();
    }

    /**
     * Create an instance of {@link Batch }
     * 
     */
    public Batch createBatch() {
        return new Batch();
    }

    /**
     * Create an instance of {@link DiscrepancyReason }
     * 
     */
    public DiscrepancyReason createDiscrepancyReason() {
        return new DiscrepancyReason();
    }

    /**
     * Create an instance of {@link BEModificationOperation }
     * 
     */
    public BEModificationOperation createBEModificationOperation() {
        return new BEModificationOperation();
    }

    /**
     * Create an instance of {@link VeterinaryAuthentication }
     * 
     */
    public VeterinaryAuthentication createVeterinaryAuthentication() {
        return new VeterinaryAuthentication();
    }

    /**
     * Create an instance of {@link ProductionOperation }
     * 
     */
    public ProductionOperation createProductionOperation() {
        return new ProductionOperation();
    }

    /**
     * Create an instance of {@link Delivery }
     * 
     */
    public Delivery createDelivery() {
        return new Delivery();
    }

    /**
     * Create an instance of {@link BEActivityLocationsModificationOperation.ActivityLocation }
     * 
     */
    public BEActivityLocationsModificationOperation.ActivityLocation createBEActivityLocationsModificationOperationActivityLocation() {
        return new BEActivityLocationsModificationOperation.ActivityLocation();
    }

    /**
     * Create an instance of {@link BusinessEntity.ActivityLocation }
     * 
     */
    public BusinessEntity.ActivityLocation createBusinessEntityActivityLocation() {
        return new BusinessEntity.ActivityLocation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOptions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "listOptions")
    public JAXBElement<ListOptions> createListOptions(ListOptions value) {
        return new JAXBElement<ListOptions>(_ListOptions_QNAME, ListOptions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryByUuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryByUuidResponse")
    public JAXBElement<GetStockEntryByUuidResponse> createGetStockEntryByUuidResponse(GetStockEntryByUuidResponse value) {
        return new JAXBElement<GetStockEntryByUuidResponse>(_GetStockEntryByUuidResponse_QNAME, GetStockEntryByUuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTransportMovementDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateTransportMovementDetailsResponse")
    public JAXBElement<UpdateTransportMovementDetailsResponse> createUpdateTransportMovementDetailsResponse(UpdateTransportMovementDetailsResponse value) {
        return new JAXBElement<UpdateTransportMovementDetailsResponse>(_UpdateTransportMovementDetailsResponse_QNAME, UpdateTransportMovementDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAppliedUserAuthorityListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getAppliedUserAuthorityListResponse")
    public JAXBElement<GetAppliedUserAuthorityListResponse> createGetAppliedUserAuthorityListResponse(GetAppliedUserAuthorityListResponse value) {
        return new JAXBElement<GetAppliedUserAuthorityListResponse>(_GetAppliedUserAuthorityListResponse_QNAME, GetAppliedUserAuthorityListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryByGuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryByGuidResponse")
    public JAXBElement<GetStockEntryByGuidResponse> createGetStockEntryByGuidResponse(GetStockEntryByGuidResponse value) {
        return new JAXBElement<GetStockEntryByGuidResponse>(_GetStockEntryByGuidResponse_QNAME, GetStockEntryByGuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessEntityList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "businessEntityList")
    public JAXBElement<BusinessEntityList> createBusinessEntityList(BusinessEntityList value) {
        return new JAXBElement<BusinessEntityList>(_BusinessEntityList_QNAME, BusinessEntityList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "requestRejectedFault")
    public JAXBElement<FaultInfo> createRequestRejectedFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_RequestRejectedFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StockEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "stockEntry")
    public JAXBElement<StockEntry> createStockEntry(StockEntry value) {
        return new JAXBElement<StockEntry>(_StockEntry_QNAME, StockEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionalizationShippingRuleList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nShippingRuleList")
    public JAXBElement<RegionalizationShippingRuleList> createR13NShippingRuleList(RegionalizationShippingRuleList value) {
        return new JAXBElement<RegionalizationShippingRuleList>(_R13NShippingRuleList_QNAME, RegionalizationShippingRuleList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StockEntryList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "stockEntryList")
    public JAXBElement<StockEntryList> createStockEntryList(StockEntryList value) {
        return new JAXBElement<StockEntryList>(_StockEntryList_QNAME, StockEntryList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryListResponse")
    public JAXBElement<GetStockEntryListResponse> createGetStockEntryListResponse(GetStockEntryListResponse value) {
        return new JAXBElement<GetStockEntryListResponse>(_GetStockEntryListResponse_QNAME, GetStockEntryListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "regionList")
    public JAXBElement<RegionList> createRegionList(RegionList value) {
        return new JAXBElement<RegionList>(_RegionList_QNAME, RegionList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnimalDisease }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "disease")
    public JAXBElement<AnimalDisease> createDisease(AnimalDisease value) {
        return new JAXBElement<AnimalDisease>(_Disease_QNAME, AnimalDisease.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productType")
    public JAXBElement<BigInteger> createProductType(BigInteger value) {
        return new JAXBElement<BigInteger>(_ProductType_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEnterpriseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyEnterpriseRequest")
    public JAXBElement<ModifyEnterpriseRequest> createModifyEnterpriseRequest(ModifyEnterpriseRequest value) {
        return new JAXBElement<ModifyEnterpriseRequest>(_ModifyEnterpriseRequest_QNAME, ModifyEnterpriseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResearchMethodList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "researchMethodList")
    public JAXBElement<ResearchMethodList> createResearchMethodList(ResearchMethodList value) {
        return new JAXBElement<ResearchMethodList>(_ResearchMethodList_QNAME, ResearchMethodList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "businessMember")
    public JAXBElement<BusinessMember> createBusinessMember(BusinessMember value) {
        return new JAXBElement<BusinessMember>(_BusinessMember_QNAME, BusinessMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterProductionOperationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "registerProductionOperationResponse")
    public JAXBElement<RegisterProductionOperationResponse> createRegisterProductionOperationResponse(RegisterProductionOperationResponse value) {
        return new JAXBElement<RegisterProductionOperationResponse>(_RegisterProductionOperationResponse_QNAME, RegisterProductionOperationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "subProductList")
    public JAXBElement<SubProductList> createSubProductList(SubProductList value) {
        return new JAXBElement<SubProductList>(_SubProductList_QNAME, SubProductList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResearchMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "researchMethod")
    public JAXBElement<ResearchMethod> createResearchMethod(ResearchMethod value) {
        return new JAXBElement<ResearchMethod>(_ResearchMethod_QNAME, ResearchMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnitList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "unitList")
    public JAXBElement<UnitList> createUnitList(UnitList value) {
        return new JAXBElement<UnitList>(_UnitList_QNAME, UnitList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "countryGuid")
    public JAXBElement<String> createCountryGuid(String value) {
        return new JAXBElement<String>(_CountryGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "regionGuid")
    public JAXBElement<String> createRegionGuid(String value) {
        return new JAXBElement<String>(_RegionGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Application }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application", name = "application")
    public JAXBElement<Application> createApplication(Application value) {
        return new JAXBElement<Application>(_Application_QNAME, Application.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserWorkingAreasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateUserWorkingAreasResponse")
    public JAXBElement<UpdateUserWorkingAreasResponse> createUpdateUserWorkingAreasResponse(UpdateUserWorkingAreasResponse value) {
        return new JAXBElement<UpdateUserWorkingAreasResponse>(_UpdateUserWorkingAreasResponse_QNAME, UpdateUserWorkingAreasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessIncomingConsignmentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "processIncomingConsignmentResponse")
    public JAXBElement<ProcessIncomingConsignmentResponse> createProcessIncomingConsignmentResponse(ProcessIncomingConsignmentResponse value) {
        return new JAXBElement<ProcessIncomingConsignmentResponse>(_ProcessIncomingConsignmentResponse_QNAME, ProcessIncomingConsignmentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyActivityLocationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyActivityLocationsResponse")
    public JAXBElement<ModifyActivityLocationsResponse> createModifyActivityLocationsResponse(ModifyActivityLocationsResponse value) {
        return new JAXBElement<ModifyActivityLocationsResponse>(_ModifyActivityLocationsResponse_QNAME, ModifyActivityLocationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Purpose }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "purpose")
    public JAXBElement<Purpose> createPurpose(Purpose value) {
        return new JAXBElement<Purpose>(_Purpose_QNAME, Purpose.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVetDocumentChangesListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getVetDocumentChangesListRequest")
    public JAXBElement<GetVetDocumentChangesListRequest> createGetVetDocumentChangesListRequest(GetVetDocumentChangesListRequest value) {
        return new JAXBElement<GetVetDocumentChangesListRequest>(_GetVetDocumentChangesListRequest_QNAME, GetVetDocumentChangesListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurposeList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "purposeList")
    public JAXBElement<PurposeList> createPurposeList(PurposeList value) {
        return new JAXBElement<PurposeList>(_PurposeList_QNAME, PurposeList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Region }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "region")
    public JAXBElement<Region> createRegion(Region value) {
        return new JAXBElement<Region>(_Region_QNAME, Region.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyBusinessEntityRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyBusinessEntityRequest")
    public JAXBElement<ModifyBusinessEntityRequest> createModifyBusinessEntityRequest(ModifyBusinessEntityRequest value) {
        return new JAXBElement<ModifyBusinessEntityRequest>(_ModifyBusinessEntityRequest_QNAME, ModifyBusinessEntityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StreetList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "streetList")
    public JAXBElement<StreetList> createStreetList(StreetList value) {
        return new JAXBElement<StreetList>(_StreetList_QNAME, StreetList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareOutgoingConsignmentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "prepareOutgoingConsignmentResponse")
    public JAXBElement<PrepareOutgoingConsignmentResponse> createPrepareOutgoingConsignmentResponse(PrepareOutgoingConsignmentResponse value) {
        return new JAXBElement<PrepareOutgoingConsignmentResponse>(_PrepareOutgoingConsignmentResponse_QNAME, PrepareOutgoingConsignmentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindBusinessEntityUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "unbindBusinessEntityUserResponse")
    public JAXBElement<UnbindBusinessEntityUserResponse> createUnbindBusinessEntityUserResponse(UnbindBusinessEntityUserResponse value) {
        return new JAXBElement<UnbindBusinessEntityUserResponse>(_UnbindBusinessEntityUserResponse_QNAME, UnbindBusinessEntityUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "subProductGuid")
    public JAXBElement<String> createSubProductGuid(String value) {
        return new JAXBElement<String>(_SubProductGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorityList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "authorityList")
    public JAXBElement<AuthorityList> createAuthorityList(AuthorityList value) {
        return new JAXBElement<AuthorityList>(_AuthorityList_QNAME, AuthorityList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link District }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "district")
    public JAXBElement<District> createDistrict(District value) {
        return new JAXBElement<District>(_District_QNAME, District.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Unit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "unit")
    public JAXBElement<Unit> createUnit(Unit value) {
        return new JAXBElement<Unit>(_Unit_QNAME, Unit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessEntityUserRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getBusinessEntityUserRequest")
    public JAXBElement<GetBusinessEntityUserRequest> createGetBusinessEntityUserRequest(GetBusinessEntityUserRequest value) {
        return new JAXBElement<GetBusinessEntityUserRequest>(_GetBusinessEntityUserRequest_QNAME, GetBusinessEntityUserRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productItem")
    public JAXBElement<ProductItem> createProductItem(ProductItem value) {
        return new JAXBElement<ProductItem>(_ProductItem_QNAME, ProductItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "subProduct")
    public JAXBElement<SubProduct> createSubProduct(SubProduct value) {
        return new JAXBElement<SubProduct>(_SubProduct_QNAME, SubProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeStockEntriesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "mergeStockEntriesRequest")
    public JAXBElement<MergeStockEntriesRequest> createMergeStockEntriesRequest(MergeStockEntriesRequest value) {
        return new JAXBElement<MergeStockEntriesRequest>(_MergeStockEntriesRequest_QNAME, MergeStockEntriesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountryList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "countryList")
    public JAXBElement<CountryList> createCountryList(CountryList value) {
        return new JAXBElement<CountryList>(_CountryList_QNAME, CountryList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationResultData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application", name = "applicationResultData")
    public JAXBElement<ApplicationResultData> createApplicationResultData(ApplicationResultData value) {
        return new JAXBElement<ApplicationResultData>(_ApplicationResultData_QNAME, ApplicationResultData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "entityNotFoundFault")
    public JAXBElement<FaultInfo> createEntityNotFoundFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_EntityNotFoundFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindBusinessEntityUserRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "unbindBusinessEntityUserRequest")
    public JAXBElement<UnbindBusinessEntityUserRequest> createUnbindBusinessEntityUserRequest(UnbindBusinessEntityUserRequest value) {
        return new JAXBElement<UnbindBusinessEntityUserRequest>(_UnbindBusinessEntityUserRequest_QNAME, UnbindBusinessEntityUserRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEnterpriseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyEnterpriseResponse")
    public JAXBElement<ModifyEnterpriseResponse> createModifyEnterpriseResponse(ModifyEnterpriseResponse value) {
        return new JAXBElement<ModifyEnterpriseResponse>(_ModifyEnterpriseResponse_QNAME, ModifyEnterpriseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterpriseGroup")
    public JAXBElement<BigInteger> createEnterpriseGroup(BigInteger value) {
        return new JAXBElement<BigInteger>(_EnterpriseGroup_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResolveDiscrepancyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "resolveDiscrepancyRequest")
    public JAXBElement<ResolveDiscrepancyRequest> createResolveDiscrepancyRequest(ResolveDiscrepancyRequest value) {
        return new JAXBElement<ResolveDiscrepancyRequest>(_ResolveDiscrepancyRequest_QNAME, ResolveDiscrepancyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserAuthoritiesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateUserAuthoritiesRequest")
    public JAXBElement<UpdateUserAuthoritiesRequest> createUpdateUserAuthoritiesRequest(UpdateUserAuthoritiesRequest value) {
        return new JAXBElement<UpdateUserAuthoritiesRequest>(_UpdateUserAuthoritiesRequest_QNAME, UpdateUserAuthoritiesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVetDocumentListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getVetDocumentListResponse")
    public JAXBElement<GetVetDocumentListResponse> createGetVetDocumentListResponse(GetVetDocumentListResponse value) {
        return new JAXBElement<GetVetDocumentListResponse>(_GetVetDocumentListResponse_QNAME, GetVetDocumentListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateVeterinaryEventsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateVeterinaryEventsResponse")
    public JAXBElement<UpdateVeterinaryEventsResponse> createUpdateVeterinaryEventsResponse(UpdateVeterinaryEventsResponse value) {
        return new JAXBElement<UpdateVeterinaryEventsResponse>(_UpdateVeterinaryEventsResponse_QNAME, UpdateVeterinaryEventsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VetDocumentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "vetDocumentType")
    public JAXBElement<VetDocumentType> createVetDocumentType(VetDocumentType value) {
        return new JAXBElement<VetDocumentType>(_VetDocumentType_QNAME, VetDocumentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RouteSectionR13NRules }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "r13nRouteSection")
    public JAXBElement<RouteSectionR13NRules> createR13NRouteSection(RouteSectionR13NRules value) {
        return new JAXBElement<RouteSectionR13NRules>(_R13NRouteSection_QNAME, RouteSectionR13NRules.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessEntityUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getBusinessEntityUserResponse")
    public JAXBElement<GetBusinessEntityUserResponse> createGetBusinessEntityUserResponse(GetBusinessEntityUserResponse value) {
        return new JAXBElement<GetBusinessEntityUserResponse>(_GetBusinessEntityUserResponse_QNAME, GetBusinessEntityUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductItemList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productItemList")
    public JAXBElement<ProductItemList> createProductItemList(ProductItemList value) {
        return new JAXBElement<ProductItemList>(_ProductItemList_QNAME, ProductItemList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserWorkingAreasRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateUserWorkingAreasRequest")
    public JAXBElement<UpdateUserWorkingAreasRequest> createUpdateUserWorkingAreasRequest(UpdateUserWorkingAreasRequest value) {
        return new JAXBElement<UpdateUserWorkingAreasRequest>(_UpdateUserWorkingAreasRequest_QNAME, UpdateUserWorkingAreasRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application", name = "applicationData")
    public JAXBElement<ApplicationData> createApplicationData(ApplicationData value) {
        return new JAXBElement<ApplicationData>(_ApplicationData_QNAME, ApplicationData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "stockEntryUuid")
    public JAXBElement<String> createStockEntryUuid(String value) {
        return new JAXBElement<String>(_StockEntryUuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterProductionOperationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "registerProductionOperationRequest")
    public JAXBElement<RegisterProductionOperationRequest> createRegisterProductionOperationRequest(RegisterProductionOperationRequest value) {
        return new JAXBElement<RegisterProductionOperationRequest>(_RegisterProductionOperationRequest_QNAME, RegisterProductionOperationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Country }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "country")
    public JAXBElement<Country> createCountry(Country value) {
        return new JAXBElement<Country>(_Country_QNAME, Country.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryVersionListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryVersionListRequest")
    public JAXBElement<GetStockEntryVersionListRequest> createGetStockEntryVersionListRequest(GetStockEntryVersionListRequest value) {
        return new JAXBElement<GetStockEntryVersionListRequest>(_GetStockEntryVersionListRequest_QNAME, GetStockEntryVersionListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "deliveryParticipant")
    public JAXBElement<BusinessMember> createDeliveryParticipant(BusinessMember value) {
        return new JAXBElement<BusinessMember>(_DeliveryParticipant_QNAME, BusinessMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateVeterinaryEventsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateVeterinaryEventsRequest")
    public JAXBElement<UpdateVeterinaryEventsRequest> createUpdateVeterinaryEventsRequest(UpdateVeterinaryEventsRequest value) {
        return new JAXBElement<UpdateVeterinaryEventsRequest>(_UpdateVeterinaryEventsRequest_QNAME, UpdateVeterinaryEventsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "uuid")
    public JAXBElement<String> createUuid(String value) {
        return new JAXBElement<String>(_Uuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBusinessEntityUserRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "addBusinessEntityUserRequest")
    public JAXBElement<AddBusinessEntityUserRequest> createAddBusinessEntityUserRequest(AddBusinessEntityUserRequest value) {
        return new JAXBElement<AddBusinessEntityUserRequest>(_AddBusinessEntityUserRequest_QNAME, AddBusinessEntityUserRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessEntityUserListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getBusinessEntityUserListResponse")
    public JAXBElement<GetBusinessEntityUserListResponse> createGetBusinessEntityUserListResponse(GetBusinessEntityUserListResponse value) {
        return new JAXBElement<GetBusinessEntityUserListResponse>(_GetBusinessEntityUserListResponse_QNAME, GetBusinessEntityUserListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserAuthoritiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateUserAuthoritiesResponse")
    public JAXBElement<UpdateUserAuthoritiesResponse> createUpdateUserAuthoritiesResponse(UpdateUserAuthoritiesResponse value) {
        return new JAXBElement<UpdateUserAuthoritiesResponse>(_UpdateUserAuthoritiesResponse_QNAME, UpdateUserAuthoritiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "districtGuid")
    public JAXBElement<String> createDistrictGuid(String value) {
        return new JAXBElement<String>(_DistrictGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application/ws-definitions", name = "unsupportedApplicationDataTypeFault")
    public JAXBElement<FaultInfo> createUnsupportedApplicationDataTypeFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_UnsupportedApplicationDataTypeFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessEntityUserListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getBusinessEntityUserListRequest")
    public JAXBElement<GetBusinessEntityUserListRequest> createGetBusinessEntityUserListRequest(GetBusinessEntityUserListRequest value) {
        return new JAXBElement<GetBusinessEntityUserListRequest>(_GetBusinessEntityUserListRequest_QNAME, GetBusinessEntityUserListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "internalServiceFault")
    public JAXBElement<FaultInfo> createInternalServiceFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_InternalServiceFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WithdrawVetDocumentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "withdrawVetDocumentRequest")
    public JAXBElement<WithdrawVetDocumentRequest> createWithdrawVetDocumentRequest(WithdrawVetDocumentRequest value) {
        return new JAXBElement<WithdrawVetDocumentRequest>(_WithdrawVetDocumentRequest_QNAME, WithdrawVetDocumentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VetDocumentStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "vetDocumentStatus")
    public JAXBElement<VetDocumentStatus> createVetDocumentStatus(VetDocumentStatus value) {
        return new JAXBElement<VetDocumentStatus>(_VetDocumentStatus_QNAME, VetDocumentStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "incorrectRequestFault")
    public JAXBElement<FaultInfo> createIncorrectRequestFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_IncorrectRequestFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryChangesListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryChangesListRequest")
    public JAXBElement<GetStockEntryChangesListRequest> createGetStockEntryChangesListRequest(GetStockEntryChangesListRequest value) {
        return new JAXBElement<GetStockEntryChangesListRequest>(_GetStockEntryChangesListRequest_QNAME, GetStockEntryChangesListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Product }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "product")
    public JAXBElement<Product> createProduct(Product value) {
        return new JAXBElement<Product>(_Product_QNAME, Product.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionalizationConditionList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nConditionList")
    public JAXBElement<RegionalizationConditionList> createR13NConditionList(RegionalizationConditionList value) {
        return new JAXBElement<RegionalizationConditionList>(_R13NConditionList_QNAME, RegionalizationConditionList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegionalizationRegionStatusList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nRegionStatusList")
    public JAXBElement<RegionalizationRegionStatusList> createR13NRegionStatusList(RegionalizationRegionStatusList value) {
        return new JAXBElement<RegionalizationRegionStatusList>(_R13NRegionStatusList_QNAME, RegionalizationRegionStatusList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application/ws-definitions", name = "unknownServiceIdFault")
    public JAXBElement<FaultInfo> createUnknownServiceIdFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_UnknownServiceIdFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBusinessEntityUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "addBusinessEntityUserResponse")
    public JAXBElement<AddBusinessEntityUserResponse> createAddBusinessEntityUserResponse(AddBusinessEntityUserResponse value) {
        return new JAXBElement<AddBusinessEntityUserResponse>(_AddBusinessEntityUserResponse_QNAME, AddBusinessEntityUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessErrorList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application", name = "errorList")
    public JAXBElement<BusinessErrorList> createErrorList(BusinessErrorList value) {
        return new JAXBElement<BusinessErrorList>(_ErrorList_QNAME, BusinessErrorList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyBusinessEntityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyBusinessEntityResponse")
    public JAXBElement<ModifyBusinessEntityResponse> createModifyBusinessEntityResponse(ModifyBusinessEntityResponse value) {
        return new JAXBElement<ModifyBusinessEntityResponse>(_ModifyBusinessEntityResponse_QNAME, ModifyBusinessEntityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityLocationList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "activityLocationList")
    public JAXBElement<ActivityLocationList> createActivityLocationList(ActivityLocationList value) {
        return new JAXBElement<ActivityLocationList>(_ActivityLocationList_QNAME, ActivityLocationList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnterpriseList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterpriseList")
    public JAXBElement<EnterpriseList> createEnterpriseList(EnterpriseList value) {
        return new JAXBElement<EnterpriseList>(_EnterpriseList_QNAME, EnterpriseList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "businessEntity")
    public JAXBElement<BusinessEntity> createBusinessEntity(BusinessEntity value) {
        return new JAXBElement<BusinessEntity>(_BusinessEntity_QNAME, BusinessEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "globalID")
    public JAXBElement<String> createGlobalID(String value) {
        return new JAXBElement<String>(_GlobalID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Enterprise }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterprise")
    public JAXBElement<Enterprise> createEnterprise(Enterprise value) {
        return new JAXBElement<Enterprise>(_Enterprise_QNAME, Enterprise.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "userList")
    public JAXBElement<UserList> createUserList(UserList value) {
        return new JAXBElement<UserList>(_UserList_QNAME, UserList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "enterpriseGuid")
    public JAXBElement<String> createEnterpriseGuid(String value) {
        return new JAXBElement<String>(_EnterpriseGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateInterval }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base", name = "updateDateInterval")
    public JAXBElement<DateInterval> createUpdateDateInterval(DateInterval value) {
        return new JAXBElement<DateInterval>(_UpdateDateInterval_QNAME, DateInterval.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryVersionListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryVersionListResponse")
    public JAXBElement<GetStockEntryVersionListResponse> createGetStockEntryVersionListResponse(GetStockEntryVersionListResponse value) {
        return new JAXBElement<GetStockEntryVersionListResponse>(_GetStockEntryVersionListResponse_QNAME, GetStockEntryVersionListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeStockEntriesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "mergeStockEntriesResponse")
    public JAXBElement<MergeStockEntriesResponse> createMergeStockEntriesResponse(MergeStockEntriesResponse value) {
        return new JAXBElement<MergeStockEntriesResponse>(_MergeStockEntriesResponse_QNAME, MergeStockEntriesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "cargoType")
    public JAXBElement<SubProduct> createCargoType(SubProduct value) {
        return new JAXBElement<SubProduct>(_CargoType_QNAME, SubProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyProducerStockListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyProducerStockListRequest")
    public JAXBElement<ModifyProducerStockListRequest> createModifyProducerStockListRequest(ModifyProducerStockListRequest value) {
        return new JAXBElement<ModifyProducerStockListRequest>(_ModifyProducerStockListRequest_QNAME, ModifyProducerStockListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productList")
    public JAXBElement<ProductList> createProductList(ProductList value) {
        return new JAXBElement<ProductList>(_ProductList_QNAME, ProductList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "accessDeniedFault")
    public JAXBElement<FaultInfo> createAccessDeniedFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_AccessDeniedFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResolveDiscrepancyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "resolveDiscrepancyResponse")
    public JAXBElement<ResolveDiscrepancyResponse> createResolveDiscrepancyResponse(ResolveDiscrepancyResponse value) {
        return new JAXBElement<ResolveDiscrepancyResponse>(_ResolveDiscrepancyResponse_QNAME, ResolveDiscrepancyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVetDocumentByUuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getVetDocumentByUuidResponse")
    public JAXBElement<GetVetDocumentByUuidResponse> createGetVetDocumentByUuidResponse(GetVetDocumentByUuidResponse value) {
        return new JAXBElement<GetVetDocumentByUuidResponse>(_GetVetDocumentByUuidResponse_QNAME, GetVetDocumentByUuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalityList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "localityList")
    public JAXBElement<LocalityList> createLocalityList(LocalityList value) {
        return new JAXBElement<LocalityList>(_LocalityList_QNAME, LocalityList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessIncomingConsignmentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "processIncomingConsignmentRequest")
    public JAXBElement<ProcessIncomingConsignmentRequest> createProcessIncomingConsignmentRequest(ProcessIncomingConsignmentRequest value) {
        return new JAXBElement<ProcessIncomingConsignmentRequest>(_ProcessIncomingConsignmentRequest_QNAME, ProcessIncomingConsignmentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTransportMovementDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "updateTransportMovementDetailsRequest")
    public JAXBElement<UpdateTransportMovementDetailsRequest> createUpdateTransportMovementDetailsRequest(UpdateTransportMovementDetailsRequest value) {
        return new JAXBElement<UpdateTransportMovementDetailsRequest>(_UpdateTransportMovementDetailsRequest_QNAME, UpdateTransportMovementDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/base/ws-definitions", name = "offsetOutOfRangeFault")
    public JAXBElement<FaultInfo> createOffsetOutOfRangeFault(FaultInfo value) {
        return new JAXBElement<FaultInfo>(_OffsetOutOfRangeFault_QNAME, FaultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VetDocumentList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "vetDocumentList")
    public JAXBElement<VetDocumentList> createVetDocumentList(VetDocumentList value) {
        return new JAXBElement<VetDocumentList>(_VetDocumentList_QNAME, VetDocumentList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StockEntryEventList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "stockEntryEventList")
    public JAXBElement<StockEntryEventList> createStockEntryEventList(StockEntryEventList value) {
        return new JAXBElement<StockEntryEventList>(_StockEntryEventList_QNAME, StockEntryEventList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryByGuidRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryByGuidRequest")
    public JAXBElement<GetStockEntryByGuidRequest> createGetStockEntryByGuidRequest(GetStockEntryByGuidRequest value) {
        return new JAXBElement<GetStockEntryByGuidRequest>(_GetStockEntryByGuidRequest_QNAME, GetStockEntryByGuidRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Area }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "r13nZone")
    public JAXBElement<Area> createR13NZone(Area value) {
        return new JAXBElement<Area>(_R13NZone_QNAME, Area.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareOutgoingConsignmentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "prepareOutgoingConsignmentRequest")
    public JAXBElement<PrepareOutgoingConsignmentRequest> createPrepareOutgoingConsignmentRequest(PrepareOutgoingConsignmentRequest value) {
        return new JAXBElement<PrepareOutgoingConsignmentRequest>(_PrepareOutgoingConsignmentRequest_QNAME, PrepareOutgoingConsignmentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VetDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "vetDocument")
    public JAXBElement<VetDocument> createVetDocument(VetDocument value) {
        return new JAXBElement<VetDocument>(_VetDocument_QNAME, VetDocument.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyProducerStockListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyProducerStockListResponse")
    public JAXBElement<ModifyProducerStockListResponse> createModifyProducerStockListResponse(ModifyProducerStockListResponse value) {
        return new JAXBElement<ModifyProducerStockListResponse>(_ModifyProducerStockListResponse_QNAME, ModifyProducerStockListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVetDocumentByUuidRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getVetDocumentByUuidRequest")
    public JAXBElement<GetVetDocumentByUuidRequest> createGetVetDocumentByUuidRequest(GetVetDocumentByUuidRequest value) {
        return new JAXBElement<GetVetDocumentByUuidRequest>(_GetVetDocumentByUuidRequest_QNAME, GetVetDocumentByUuidRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckShipmentRegionalizationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "checkShipmentRegionalizationResponse")
    public JAXBElement<CheckShipmentRegionalizationResponse> createCheckShipmentRegionalizationResponse(CheckShipmentRegionalizationResponse value) {
        return new JAXBElement<CheckShipmentRegionalizationResponse>(_CheckShipmentRegionalizationResponse_QNAME, CheckShipmentRegionalizationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVetDocumentListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getVetDocumentListRequest")
    public JAXBElement<GetVetDocumentListRequest> createGetVetDocumentListRequest(GetVetDocumentListRequest value) {
        return new JAXBElement<GetVetDocumentListRequest>(_GetVetDocumentListRequest_QNAME, GetVetDocumentListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentRoute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "shipmentRoute")
    public JAXBElement<ShipmentRoute> createShipmentRoute(ShipmentRoute value) {
        return new JAXBElement<ShipmentRoute>(_ShipmentRoute_QNAME, ShipmentRoute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "localityGuid")
    public JAXBElement<String> createLocalityGuid(String value) {
        return new JAXBElement<String>(_LocalityGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryListRequest")
    public JAXBElement<GetStockEntryListRequest> createGetStockEntryListRequest(GetStockEntryListRequest value) {
        return new JAXBElement<GetStockEntryListRequest>(_GetStockEntryListRequest_QNAME, GetStockEntryListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryChangesListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryChangesListResponse")
    public JAXBElement<GetStockEntryChangesListResponse> createGetStockEntryChangesListResponse(GetStockEntryChangesListResponse value) {
        return new JAXBElement<GetStockEntryChangesListResponse>(_GetStockEntryChangesListResponse_QNAME, GetStockEntryChangesListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyActivityLocationsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "modifyActivityLocationsRequest")
    public JAXBElement<ModifyActivityLocationsRequest> createModifyActivityLocationsRequest(ModifyActivityLocationsRequest value) {
        return new JAXBElement<ModifyActivityLocationsRequest>(_ModifyActivityLocationsRequest_QNAME, ModifyActivityLocationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DistrictList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "districtList")
    public JAXBElement<DistrictList> createDistrictList(DistrictList value) {
        return new JAXBElement<DistrictList>(_DistrictList_QNAME, DistrictList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", name = "vetDocumentUuid")
    public JAXBElement<String> createVetDocumentUuid(String value) {
        return new JAXBElement<String>(_VetDocumentUuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnimalDiseaseList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "diseaseList")
    public JAXBElement<AnimalDiseaseList> createDiseaseList(AnimalDiseaseList value) {
        return new JAXBElement<AnimalDiseaseList>(_DiseaseList_QNAME, AnimalDiseaseList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAppliedUserAuthorityListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getAppliedUserAuthorityListRequest")
    public JAXBElement<GetAppliedUserAuthorityListRequest> createGetAppliedUserAuthorityListRequest(GetAppliedUserAuthorityListRequest value) {
        return new JAXBElement<GetAppliedUserAuthorityListRequest>(_GetAppliedUserAuthorityListRequest_QNAME, GetAppliedUserAuthorityListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", name = "productGuid")
    public JAXBElement<String> createProductGuid(String value) {
        return new JAXBElement<String>(_ProductGuid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WithdrawVetDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "withdrawVetDocumentResponse")
    public JAXBElement<WithdrawVetDocumentResponse> createWithdrawVetDocumentResponse(WithdrawVetDocumentResponse value) {
        return new JAXBElement<WithdrawVetDocumentResponse>(_WithdrawVetDocumentResponse_QNAME, WithdrawVetDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckShipmentRegionalizationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "checkShipmentRegionalizationRequest")
    public JAXBElement<CheckShipmentRegionalizationRequest> createCheckShipmentRegionalizationRequest(CheckShipmentRegionalizationRequest value) {
        return new JAXBElement<CheckShipmentRegionalizationRequest>(_CheckShipmentRegionalizationRequest_QNAME, CheckShipmentRegionalizationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application", name = "businessError")
    public JAXBElement<BusinessError> createBusinessError(BusinessError value) {
        return new JAXBElement<BusinessError>(_BusinessError_QNAME, BusinessError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStockEntryByUuidRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getStockEntryByUuidRequest")
    public JAXBElement<GetStockEntryByUuidRequest> createGetStockEntryByUuidRequest(GetStockEntryByUuidRequest value) {
        return new JAXBElement<GetStockEntryByUuidRequest>(_GetStockEntryByUuidRequest_QNAME, GetStockEntryByUuidRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/application", name = "binary")
    public JAXBElement<byte[]> createBinary(byte[] value) {
        return new JAXBElement<byte[]>(_Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVetDocumentChangesListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", name = "getVetDocumentChangesListResponse")
    public JAXBElement<GetVetDocumentChangesListResponse> createGetVetDocumentChangesListResponse(GetVetDocumentChangesListResponse value) {
        return new JAXBElement<GetVetDocumentChangesListResponse>(_GetVetDocumentChangesListResponse_QNAME, GetVetDocumentChangesListResponse.class, null, value);
    }

}
