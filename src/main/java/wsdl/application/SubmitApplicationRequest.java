
package wsdl.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiKey" type="{http://api.vetrf.ru/schema/cdm/application}APIKey"/>
 *         &lt;element ref="{http://api.vetrf.ru/schema/cdm/application}application"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apiKey",
    "application"
})
@XmlRootElement(name = "submitApplicationRequest", namespace = "http://api.vetrf.ru/schema/cdm/application/ws-definitions")
public class SubmitApplicationRequest {

    @XmlElement(namespace = "http://api.vetrf.ru/schema/cdm/application/ws-definitions", required = true)
    protected String apiKey;
    @XmlElement(namespace = "http://api.vetrf.ru/schema/cdm/application", required = true)
    protected Application application;

    /**
     * Gets the value of the apiKey property.
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * Sets the value of the apiKey property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setApiKey(String value) {
        this.apiKey = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible application is
     *     {@link Application }
     *     
     */
    public Application getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     *
     * @param value
     *     allowed application is
     *     {@link Application }
     *     
     */
    public void setApplication(Application value) {
        this.application = value;
    }

}
