
package wsdl.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Базовый тип для описания списков сущностей.
 *          
 * 
 * <p>Java class for EntityList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="total" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="offset" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="hasMore" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityList", namespace = "http://api.vetrf.ru/schema/cdm/base")
@XmlSeeAlso({
    UnitList.class,
    ProductItemList.class,
    AnimalDiseaseList.class,
    EnterpriseList.class,
    SubProductList.class,
    DistrictList.class,
    ActivityLocationList.class,
    ResearchMethodList.class,
    RegionalizationRegionStatusList.class,
    RegionalizationConditionList.class,
    RegionList.class,
    RegionalizationShippingRuleList.class,
    CountryList.class,
    BusinessEntityList.class,
    StreetList.class,
    PurposeList.class,
    LocalityList.class,
    ProductList.class,
    EnterpriseActivityList.class,
    VetDocumentList.class,
    UserList.class,
    StockEntryList.class,
    WorkingAreaList.class
})
public class EntityList {

    @XmlAttribute(name = "count")
    protected Integer count;
    @XmlAttribute(name = "total")
    protected Long total;
    @XmlAttribute(name = "offset")
    protected Integer offset;
    @XmlAttribute(name = "hasMore")
    protected Boolean hasMore;

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible application is
     *     {@link Integer }
     *     
     */
    public Integer getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed application is
     *     {@link Integer }
     *     
     */
    public void setCount(Integer value) {
        this.count = value;
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible application is
     *     {@link Long }
     *     
     */
    public Long getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed application is
     *     {@link Long }
     *     
     */
    public void setTotal(Long value) {
        this.total = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible application is
     *     {@link Integer }
     *     
     */
    public Integer getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed application is
     *     {@link Integer }
     *     
     */
    public void setOffset(Integer value) {
        this.offset = value;
    }

    /**
     * Gets the value of the hasMore property.
     * 
     * @return
     *     possible application is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasMore() {
        return hasMore;
    }

    /**
     * Sets the value of the hasMore property.
     * 
     * @param value
     *     allowed application is
     *     {@link Boolean }
     *     
     */
    public void setHasMore(Boolean value) {
        this.hasMore = value;
    }

}
