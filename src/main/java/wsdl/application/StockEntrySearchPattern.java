
package wsdl.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StockEntrySearchPattern complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StockEntrySearchPattern">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2}StockEntry">
 *       &lt;sequence>
 *         &lt;element name="blankFilter" type="{http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2}StockEntryBlankFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StockEntrySearchPattern", namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", propOrder = {
    "blankFilter"
})
public class StockEntrySearchPattern
    extends StockEntry
{

    @XmlSchemaType(name = "string")
    protected StockEntryBlankFilter blankFilter;

    /**
     * Gets the value of the blankFilter property.
     * 
     * @return
     *     possible application is
     *     {@link StockEntryBlankFilter }
     *     
     */
    public StockEntryBlankFilter getBlankFilter() {
        return blankFilter;
    }

    /**
     * Sets the value of the blankFilter property.
     * 
     * @param value
     *     allowed application is
     *     {@link StockEntryBlankFilter }
     *     
     */
    public void setBlankFilter(StockEntryBlankFilter value) {
        this.blankFilter = value;
    }

}
