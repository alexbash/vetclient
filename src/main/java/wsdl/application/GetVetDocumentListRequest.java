
package wsdl.application;

import javax.xml.bind.annotation.*;


/**
 * Заявка на получение списка записей ветсертификатов.
 * 
 * <p>Java class for GetVetDocumentListRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetVetDocumentListRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2}MercuryApplicationRequest">
 *       &lt;sequence>
 *         &lt;element ref="{http://api.vetrf.ru/schema/cdm/base}listOptions" minOccurs="0"/>
 *         &lt;element ref="{http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2}vetDocumentType" minOccurs="0"/>
 *         &lt;element ref="{http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2}vetDocumentStatus" minOccurs="0"/>
 *         &lt;element ref="{http://api.vetrf.ru/schema/cdm/dictionary/v2}enterpriseGuid"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVetDocumentListRequest", namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2", propOrder = {
    "listOptions",
    "vetDocumentType",
    "vetDocumentStatus",
    "enterpriseGuid"
})
@XmlRootElement (name = "getVetDocumentListRequest", namespace = "http://api.vetrf.ru/schema/cdm/mercury/g2b/applications/v2")
public class GetVetDocumentListRequest
    extends MercuryApplicationRequest
{

    @XmlElement(namespace = "http://api.vetrf.ru/schema/cdm/base")
    protected ListOptions listOptions;
    @XmlElement(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2")
    @XmlSchemaType(name = "string")
    protected VetDocumentType vetDocumentType;
    @XmlElement(namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2")
    @XmlSchemaType(name = "string")
    protected VetDocumentStatus vetDocumentStatus;
    @XmlElement(namespace = "http://api.vetrf.ru/schema/cdm/dictionary/v2", required = true)
    protected String enterpriseGuid;

    /**
     * 
     *                         Параметры запрашиваемого списка.
     *                      
     * 
     * @return
     *     possible application is
     *     {@link ListOptions }
     *     
     */
    public ListOptions getListOptions() {
        return listOptions;
    }

    /**
     * Sets the value of the listOptions property.
     * 
     * @param value
     *     allowed application is
     *     {@link ListOptions }
     *     
     */
    public void setListOptions(ListOptions value) {
        this.listOptions = value;
    }

    /**
     * Тип ветсертификата.
     * 
     * @return
     *     possible application is
     *     {@link VetDocumentType }
     *     
     */
    public VetDocumentType getVetDocumentType() {
        return vetDocumentType;
    }

    /**
     * Sets the value of the vetDocumentType property.
     * 
     * @param value
     *     allowed application is
     *     {@link VetDocumentType }
     *     
     */
    public void setVetDocumentType(VetDocumentType value) {
        this.vetDocumentType = value;
    }

    /**
     * Статус ветсертификата.
     * 
     * @return
     *     possible application is
     *     {@link VetDocumentStatus }
     *     
     */
    public VetDocumentStatus getVetDocumentStatus() {
        return vetDocumentStatus;
    }

    /**
     * Sets the value of the vetDocumentStatus property.
     * 
     * @param value
     *     allowed application is
     *     {@link VetDocumentStatus }
     *     
     */
    public void setVetDocumentStatus(VetDocumentStatus value) {
        this.vetDocumentStatus = value;
    }

    /**
     * Идентификатор предприятия, по которому производится поиск записей.
     *                      
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getEnterpriseGuid() {
        return enterpriseGuid;
    }

    /**
     * Sets the value of the enterpriseGuid property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setEnterpriseGuid(String value) {
        this.enterpriseGuid = value;
    }

}
