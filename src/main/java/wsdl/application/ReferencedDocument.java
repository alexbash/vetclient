
package wsdl.application;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения о связанном документе.
 * 
 * <p>Java class for ReferencedDocument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferencedDocument">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2}Document">
 *       &lt;sequence>
 *         &lt;element name="relationshipType" type="{http://api.vetrf.ru/schema/cdm/dictionary/v2}ReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferencedDocument", namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", propOrder = {
    "relationshipType"
})
public class ReferencedDocument
    extends Document
{

    @XmlElement(required = true)
    protected BigInteger relationshipType;

    /**
     * Gets the value of the relationshipType property.
     * 
     * @return
     *     possible application is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRelationshipType() {
        return relationshipType;
    }

    /**
     * Sets the value of the relationshipType property.
     * 
     * @param value
     *     allowed application is
     *     {@link BigInteger }
     *     
     */
    public void setRelationshipType(BigInteger value) {
        this.relationshipType = value;
    }

}
