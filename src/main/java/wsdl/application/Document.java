
package wsdl.application;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Документ.
 * 
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.vetrf.ru/schema/cdm/base}GenericEntity">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://api.vetrf.ru/schema/cdm/base}String255" minOccurs="0"/>
 *         &lt;element name="form" type="{http://api.vetrf.ru/schema/cdm/base}String255" minOccurs="0"/>
 *         &lt;element name="issueSeries" type="{http://api.vetrf.ru/schema/cdm/base}String255" minOccurs="0"/>
 *         &lt;element name="issueNumber" type="{http://api.vetrf.ru/schema/cdm/base}String255" minOccurs="0"/>
 *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="type" type="{http://api.vetrf.ru/schema/cdm/dictionary/v2}DocumentType" minOccurs="0"/>
 *         &lt;element name="issuer" type="{http://api.vetrf.ru/schema/cdm/dictionary/v2}Organization" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="for" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *       &lt;attribute name="qualifier" type="{http://api.vetrf.ru/schema/cdm/base}Identifier" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "http://api.vetrf.ru/schema/cdm/mercury/vet-document/v2", propOrder = {
    "name",
    "form",
    "issueSeries",
    "issueNumber",
    "issueDate",
    "type",
    "issuer"
})
@XmlSeeAlso({
    VetDocument.class,
    DiscrepancyReport.class,
    ReferencedDocument.class,
    Waybill.class
})
public class Document
    extends GenericEntity
{

    protected String name;
    protected String form;
    protected String issueSeries;
    protected String issueNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar issueDate;
    protected BigInteger type;
    protected Organization issuer;
    @XmlAttribute(name = "for")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object _for;
    @XmlAttribute(name = "qualifier")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String qualifier;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the form property.
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getForm() {
        return form;
    }

    /**
     * Sets the value of the form property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setForm(String value) {
        this.form = value;
    }

    /**
     * Gets the value of the issueSeries property.
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getIssueSeries() {
        return issueSeries;
    }

    /**
     * Sets the value of the issueSeries property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setIssueSeries(String value) {
        this.issueSeries = value;
    }

    /**
     * Gets the value of the issueNumber property.
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getIssueNumber() {
        return issueNumber;
    }

    /**
     * Sets the value of the issueNumber property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setIssueNumber(String value) {
        this.issueNumber = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible application is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed application is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIssueDate(XMLGregorianCalendar value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible application is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed application is
     *     {@link BigInteger }
     *     
     */
    public void setType(BigInteger value) {
        this.type = value;
    }

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible application is
     *     {@link Organization }
     *     
     */
    public Organization getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed application is
     *     {@link Organization }
     *     
     */
    public void setIssuer(Organization value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the for property.
     * 
     * @return
     *     possible application is
     *     {@link Object }
     *     
     */
    public Object getFor() {
        return _for;
    }

    /**
     * Sets the value of the for property.
     * 
     * @param value
     *     allowed application is
     *     {@link Object }
     *     
     */
    public void setFor(Object value) {
        this._for = value;
    }

    /**
     * Gets the value of the qualifier property.
     * 
     * @return
     *     possible application is
     *     {@link String }
     *     
     */
    public String getQualifier() {
        return qualifier;
    }

    /**
     * Sets the value of the qualifier property.
     * 
     * @param value
     *     allowed application is
     *     {@link String }
     *     
     */
    public void setQualifier(String value) {
        this.qualifier = value;
    }

}
