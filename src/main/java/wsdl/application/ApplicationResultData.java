
package wsdl.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplicationResultData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationResultData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationResultData", namespace = "http://api.vetrf.ru/schema/cdm/application")
@XmlSeeAlso({
    UpdateVeterinaryEventsResponse.class,
    GetVetDocumentListResponse.class,
    GetStockEntryChangesListResponse.class,
    CheckShipmentRegionalizationResponse.class,
    GetVetDocumentChangesListResponse.class,
    WithdrawVetDocumentResponse.class,
    GetBusinessEntityUserResponse.class,
    PrepareOutgoingConsignmentResponse.class,
    UnbindBusinessEntityUserResponse.class,
    ResolveDiscrepancyResponse.class,
    GetVetDocumentByUuidResponse.class,
    ModifyEnterpriseResponse.class,
    ModifyProducerStockListResponse.class,
    ModifyBusinessEntityResponse.class,
    RegisterProductionOperationResponse.class,
    AddBusinessEntityUserResponse.class,
    MergeStockEntriesResponse.class,
    GetStockEntryVersionListResponse.class,
    ProcessIncomingConsignmentResponse.class,
    ModifyActivityLocationsResponse.class,
    UpdateUserWorkingAreasResponse.class,
    GetStockEntryByGuidResponse.class,
    GetAppliedUserAuthorityListResponse.class,
    UpdateTransportMovementDetailsResponse.class,
    GetStockEntryByUuidResponse.class,
    GetStockEntryListResponse.class,
    UpdateUserAuthoritiesResponse.class,
    GetBusinessEntityUserListResponse.class
})
public abstract class ApplicationResultData {


}
