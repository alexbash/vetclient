
package wsdl.dictionaryservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "DictionaryServicePortType", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/dictionary/service/v2")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface DictionaryServicePortType {


    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetPurposeByGuidResponse
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     */
    @WebMethod(operationName = "GetPurposeByGuid", action = "GetPurposeByGuid")
    @WebResult(name = "getPurposeByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetPurposeByGuidResponse getPurposeByGuid(
        @WebParam(name = "getPurposeByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetPurposeByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetPurposeByUuidResponse
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     */
    @WebMethod(operationName = "GetPurposeByUuid", action = "GetPurposeByUuid")
    @WebResult(name = "getPurposeByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetPurposeByUuidResponse getPurposeByUuid(
        @WebParam(name = "getPurposeByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetPurposeByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetPurposeListResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetPurposeList", action = "GetPurposeList")
    @WebResult(name = "getPurposeListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetPurposeListResponse getPurposeList(
        @WebParam(name = "getPurposeListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetPurposeListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetPurposeChangesListResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetPurposeChangesList", action = "GetPurposeChangesList")
    @WebResult(name = "getPurposeChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetPurposeChangesListResponse getPurposeChangesList(
        @WebParam(name = "getPurposeChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetPurposeChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetUnitByGuidResponse
     * @throws InternalServiceFault
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetUnitByGuid", action = "GetUnitByGuid")
    @WebResult(name = "getUnitByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetUnitByGuidResponse getUnitByGuid(
        @WebParam(name = "getUnitByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetUnitByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetUnitByUuidResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws EntityNotFoundFault
     */
    @WebMethod(operationName = "GetUnitByUuid", action = "GetUnitByUuid")
    @WebResult(name = "getUnitByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetUnitByUuidResponse getUnitByUuid(
        @WebParam(name = "getUnitByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetUnitByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetUnitListResponse
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetUnitList", action = "GetUnitList")
    @WebResult(name = "getUnitListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetUnitListResponse getUnitList(
        @WebParam(name = "getUnitListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetUnitListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetUnitChangesListResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetUnitChangesList", action = "GetUnitChangesList")
    @WebResult(name = "getUnitChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetUnitChangesListResponse getUnitChangesList(
        @WebParam(name = "getUnitChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetUnitChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetDiseaseByGuidResponse
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetDiseaseByGuid", action = "GetDiseaseByGuid")
    @WebResult(name = "getDiseaseByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDiseaseByGuidResponse getDiseaseByGuid(
        @WebParam(name = "getDiseaseByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDiseaseByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetDiseaseByUuidResponse
     * @throws IncorrectRequestFault
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetDiseaseByUuid", action = "GetDiseaseByUuid")
    @WebResult(name = "getDiseaseByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDiseaseByUuidResponse getDiseaseByUuid(
        @WebParam(name = "getDiseaseByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDiseaseByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetDiseaseListResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetDiseaseList", action = "GetDiseaseList")
    @WebResult(name = "getDiseaseListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDiseaseListResponse getDiseaseList(
        @WebParam(name = "getDiseaseListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDiseaseListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetDiseaseChangesListResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetDiseaseChangesList", action = "GetDiseaseChangesList")
    @WebResult(name = "getDiseaseChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDiseaseChangesListResponse getDiseaseChangesList(
        @WebParam(name = "getDiseaseChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDiseaseChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetResearchMethodByGuidResponse
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetResearchMethodByGuid", action = "GetResearchMethodByGuid")
    @WebResult(name = "getResearchMethodByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetResearchMethodByGuidResponse getResearchMethodByGuid(
        @WebParam(name = "getResearchMethodByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetResearchMethodByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetResearchMethodByUuidResponse
     * @throws IncorrectRequestFault
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetResearchMethodByUuid", action = "GetResearchMethodByUuid")
    @WebResult(name = "getResearchMethodByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetResearchMethodByUuidResponse getResearchMethodByUuid(
        @WebParam(name = "getResearchMethodByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetResearchMethodByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetResearchMethodListResponse
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetResearchMethodList", action = "GetResearchMethodList")
    @WebResult(name = "getResearchMethodListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetResearchMethodListResponse getResearchMethodList(
        @WebParam(name = "getResearchMethodListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetResearchMethodListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * 
     * @param request
     * @return
     *     returns wsdl.dictionaryservice.GetResearchMethodChangesListResponse
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     * @throws OffsetOutOfRangeFault
     */
    @WebMethod(operationName = "GetResearchMethodChangesList", action = "GetResearchMethodChangesList")
    @WebResult(name = "getResearchMethodChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetResearchMethodChangesListResponse getResearchMethodChangesList(
        @WebParam(name = "getResearchMethodChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetResearchMethodChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

}
