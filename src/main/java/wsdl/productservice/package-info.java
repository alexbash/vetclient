/**
 * 
 *         Service: ProductService
 *         Version: 2.0.4
 *     
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package wsdl.productservice;
