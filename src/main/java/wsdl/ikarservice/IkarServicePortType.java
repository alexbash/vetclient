
package wsdl.ikarservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IkarServicePortType", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ikar/service/v2")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IkarServicePortType {


    /**
     * ��������� ������ ������� ����� � ��������
     *                 ��������.
     *                 ���������� �������� ������������� ���� ��������� ������ �����,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetAllCountryListResponse
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetAllCountryList", action = "GetAllCountryList")
    @WebResult(name = "getAllCountryListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetAllCountryListResponse getAllCountryList(
        @WebParam(name = "getAllCountryListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetAllCountryListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ��������� ������ ������ ��
     *                 ����������� ��������������.
     *                 ���������� �������� ������������� ���� ��������� ������� ������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ���������, ���� ������
     *                 �� ������� �� �������
     *                 ��� ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetCountryByGuidResponse
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetCountryByGuid", action = "GetCountryByGuid")
    @WebResult(name = "getCountryByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetCountryByGuidResponse getCountryByGuid(
        @WebParam(name = "getCountryByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetCountryByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * ��������� ������ ������ �� ��������������.
     *                 ���������� �������� ������������� ���� ��������� ������� ������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ���������, ���� ������
     *                 �� ������� �� �������
     *                 ��� ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetCountryByUuidResponse
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetCountryByUuid", action = "GetCountryByUuid")
    @WebResult(name = "getCountryByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetCountryByUuidResponse getCountryByUuid(
        @WebParam(name = "getCountryByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetCountryByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * ��������� ������� ��������� � ������ �����.
     *                 ���������� �������� ������������� ���� ��������� ������ �����,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetCountryChangesListResponse
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetCountryChangesList", action = "GetCountryChangesList")
    @WebResult(name = "getCountryChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetCountryChangesListResponse getCountryChangesList(
        @WebParam(name = "getCountryChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetCountryChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� �������� � ��������
     *                 �������� �� ������.
     *                 ���������� �������� ������������� ���� ��������� ������ ��������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetRegionListByCountryResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetRegionListByCountry", action = "GetRegionListByCountry")
    @WebResult(name = "getRegionListByCountryResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetRegionListByCountryResponse getRegionListByCountry(
        @WebParam(name = "getRegionListByCountryRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetRegionListByCountryRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ��������� ������ ������� ��
     *                 ����������� ��������������.
     *                 ���������� �������� ������������� ���� ��������� ������� �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ���������, ���� ������
     *                 �� ������� �� �������
     *                 ��� ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetRegionByGuidResponse
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetRegionByGuid", action = "GetRegionByGuid")
    @WebResult(name = "getRegionByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetRegionByGuidResponse getRegionByGuid(
        @WebParam(name = "getRegionByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetRegionByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * ��������� ������ ������� �� ��������������.
     *                 ���������� �������� ������������� ���� ��������� ������� �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ���������, ���� ������
     *                 �� ������� �� �������
     *                 ��� ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetRegionByUuidResponse
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetRegionByUuid", action = "GetRegionByUuid")
    @WebResult(name = "getRegionByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetRegionByUuidResponse getRegionByUuid(
        @WebParam(name = "getRegionByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetRegionByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * ��������� ������� ��������� � ������ ��������.
     *                 ���������� �������� ������������� ���� ��������� ������ ��������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ���
     *                 ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetRegionChangesListResponse
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetRegionChangesList", action = "GetRegionChangesList")
    @WebResult(name = "getRegionChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetRegionChangesListResponse getRegionChangesList(
        @WebParam(name = "getRegionChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetRegionChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� ������� � ��������
     *                 �������� �� �������.
     *                 ���������� �������� ������������� ���� ��������� ������ �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetDistrictListByRegionResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetDistrictListByRegion", action = "GetDistrictListByRegion")
    @WebResult(name = "getDistrictListByRegionResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDistrictListByRegionResponse getDistrictListByRegion(
        @WebParam(name = "getDistrictListByRegionRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDistrictListByRegionRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ��������� ������ ������ ��
     *                 ����������� ��������������.
     *                 ���������� �������� ������������� ���� ��������� ������� ������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ���������, ���� ������
     *                 �� ������� �� �������
     *                 ��� ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetDistrictByGuidResponse
     * @throws EntityNotFoundFault
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetDistrictByGuid", action = "GetDistrictByGuid")
    @WebResult(name = "getDistrictByGuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDistrictByGuidResponse getDistrictByGuid(
        @WebParam(name = "getDistrictByGuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDistrictByGuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * ��������� ��������� ������ ������ ��
     *                 ��������������.
     *                 ���������� �������� ������������� ���� ��������� ������� ������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ���������, ���� ������
     *                 �� ������� �� �������
     *                 ��� ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetDistrictByUuidResponse
     * @throws EntityNotFoundFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetDistrictByUuid", action = "GetDistrictByUuid")
    @WebResult(name = "getDistrictByUuidResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDistrictByUuidResponse getDistrictByUuid(
        @WebParam(name = "getDistrictByUuidRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDistrictByUuidRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault
    ;

    /**
     * ��������� ������� ��������� � ������ �������.
     *                 ���������� �������� ������������� ���� ��������� ������ �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ���
     *                 ��������� ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetDistrictChangesListResponse
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetDistrictChangesList", action = "GetDistrictChangesList")
    @WebResult(name = "getDistrictChangesListResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetDistrictChangesListResponse getDistrictChangesList(
        @WebParam(name = "getDistrictChangesListRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetDistrictChangesListRequest request)
        throws IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� ���������� ������� � ��������
     *                 �������� �� �������.
     *                 ���������� �������� ������������� ���� ��������� ������ ���������� �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetLocalityListByRegionResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetLocalityListByRegion", action = "GetLocalityListByRegion")
    @WebResult(name = "getLocalityListByRegionResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetLocalityListByRegionResponse getLocalityListByRegion(
        @WebParam(name = "getLocalityListByRegionRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetLocalityListByRegionRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� ���������� ������� � ��������
     *                 �������� �� ������.
     *                 ���������� �������� ������������� ���� ��������� ������ ���������� �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetLocalityListByDistrictResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetLocalityListByDistrict", action = "GetLocalityListByDistrict")
    @WebResult(name = "getLocalityListByDistrictResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetLocalityListByDistrictResponse getLocalityListByDistrict(
        @WebParam(name = "getLocalityListByDistrictRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetLocalityListByDistrictRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ��������� ������� ���������� ������� � ��������
     *                 �������� �� ����������� ������.
     *                 ���������� �������� ������������� ���� ��������� ������ ���������� �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetLocalityListByLocalityResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "GetLocalityListByLocality", action = "GetLocalityListByLocality")
    @WebResult(name = "getLocalityListByLocalityResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetLocalityListByLocalityResponse getLocalityListByLocality(
        @WebParam(name = "getLocalityListByLocalityRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetLocalityListByLocalityRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� ���� � ��������
     *                 �������� �� �������.
     *                 ���������� �������� ������������� ���� ��������� ������ ����,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.GetStreetListByLocalityResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "GetStreetListByLocality", action = "GetStreetListByLocality")
    @WebResult(name = "getStreetListByLocalityResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public GetStreetListByLocalityResponse getStreetListByLocality(
        @WebParam(name = "getStreetListByLocalityRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        GetStreetListByLocalityRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� ���������� ������� � ��������
     *                 �������� �� ������� � �������.
     *                 ���������� �������� ������������� ���� ��������� ������ ���������� �������,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.FindLocalityListByNameResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws IncorrectRequestFault
     * @throws InternalServiceFault
     */
    @WebMethod(operationName = "FindLocalityListByName", action = "FindLocalityListByName")
    @WebResult(name = "findLocalityListByNameResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public FindLocalityListByNameResponse findLocalityListByName(
        @WebParam(name = "findLocalityListByNameRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        FindLocalityListByNameRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

    /**
     * ��������� ������ ������� ���� � ��������
     *                 �������� �� ������� � ����������� ������.
     *                 ���������� �������� ������������� ���� ��������� ������ ����,
     *                 ���� �������, � ������ ���� ��������� ������� �� ��������� ��� ���������
     *                 ���� ������ ���������� �������.
     *             
     * 
     * @param request
     * @return
     *     returns wsdl.ikarservice.FindStreetListByNameResponse
     * @throws EntityNotFoundFault
     * @throws OffsetOutOfRangeFault
     * @throws InternalServiceFault
     * @throws IncorrectRequestFault
     */
    @WebMethod(operationName = "FindStreetListByName", action = "FindStreetListByName")
    @WebResult(name = "findStreetListByNameResponse", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "response")
    public FindStreetListByNameResponse findStreetListByName(
        @WebParam(name = "findStreetListByNameRequest", targetNamespace = "http://api.vetrf.ru/schema/cdm/registry/ws-definitions/v2", partName = "request")
        FindStreetListByNameRequest request)
        throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault, OffsetOutOfRangeFault
    ;

}
