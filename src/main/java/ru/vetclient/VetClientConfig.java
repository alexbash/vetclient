package ru.vetclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import ru.vetclient.clientdictionaryservice.VetClientDictionary;
import ru.vetclient.cliententerpriseservice.VetClientEnterprise;
import ru.vetclient.clientikarservice.VetClientIkar;
import ru.vetclient.clientproductservice.VetClientProduct;
import ru.vetclient.vetclientapplications.VetClientApplication;

@Configuration
public class VetClientConfig {

    @Value("${endpointservice.application}")
    String endPointApplication;
    @Value("${endpointservice.dictionary}")
    String endPointDictionary;
    @Value("${endpointservice.enterprise}")
    String endPointEnterprise;
    @Value("${endpointservice.ikar}")
    String endPointIkar;
    @Value("${endpointservice.product}")
    String endPointProduct;


    private ClientInterceptor[] interceptors = new ClientInterceptor[] {
            new LogHttpHeaderClientInterceptor()
    };

    @Configuration

    public class WebConfig implements WebMvcConfigurer {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
            registry.addMapping("/**");
        }
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/**")
                    .addResourceLocations("/public", "classpath:/static/")
                    .setCachePeriod(31556926);

        }

        @Override
        public void addViewControllers(ViewControllerRegistry registry) {
            registry.addViewController("/").setViewName("forward:/index.html");

        }
    }

    @Bean
    public Jaxb2Marshaller marshallerApplication() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("wsdl.application");
        return marshaller;
    }

    @Bean
    public Jaxb2Marshaller marshallerEnterprise() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setContextPath("wsdl.enterpriseservice");
        return jaxb2Marshaller;
    }
    @Bean
    public Jaxb2Marshaller marshallerIkar(){
        Jaxb2Marshaller marshallerIkar = new Jaxb2Marshaller();
        marshallerIkar.setContextPath("wsdl.ikarservice");
        return marshallerIkar;
    }
    @Bean
    public Jaxb2Marshaller marshallerDictionary(){
        Jaxb2Marshaller marshallerDictionary = new Jaxb2Marshaller();
        marshallerDictionary.setContextPath("wsdl.dictionaryservice");
        return marshallerDictionary;
    }
    @Bean
    public Jaxb2Marshaller marshallerProduct(){
        Jaxb2Marshaller marshallerProduct = new Jaxb2Marshaller();
        marshallerProduct.setContextPath("wsdl.productservice");
        return marshallerProduct;
    }

    @Bean
    public VetClientApplication vetClient(Jaxb2Marshaller marshallerApplication) {
        VetClientApplication vetClientApplication = new VetClientApplication();
        vetClientApplication.setDefaultUri(endPointApplication);
        vetClientApplication.setMarshaller(marshallerApplication);
        vetClientApplication.setUnmarshaller(marshallerApplication);
        vetClientApplication.setInterceptors(interceptors);
        return vetClientApplication;
    }

    @Bean
    public VetClientEnterprise vetClientEnterprise(Jaxb2Marshaller marshallerEnterprise) {
        VetClientEnterprise vetClientEnterprise = new VetClientEnterprise();
        vetClientEnterprise.setDefaultUri(endPointEnterprise);
        vetClientEnterprise.setMarshaller(marshallerEnterprise);
        vetClientEnterprise.setUnmarshaller(marshallerEnterprise);
        vetClientEnterprise.setInterceptors(interceptors);
        return vetClientEnterprise;
    }

    @Bean
    public VetClientIkar vetClientIkar (Jaxb2Marshaller marshallerIkar){
        VetClientIkar vetClientIkar = new VetClientIkar();
        vetClientIkar.setDefaultUri(endPointIkar);
        vetClientIkar.setMarshaller(marshallerIkar);
        vetClientIkar.setUnmarshaller(marshallerIkar);
        vetClientIkar.setInterceptors(interceptors);
        return vetClientIkar;
    }
    @Bean
    public VetClientDictionary vetClientDictionary (Jaxb2Marshaller marshallerDictionary){
        VetClientDictionary vetClientDictionary = new VetClientDictionary();
        vetClientDictionary.setDefaultUri(endPointDictionary);
        vetClientDictionary.setMarshaller(marshallerDictionary);
        vetClientDictionary.setUnmarshaller(marshallerDictionary);
        vetClientDictionary.setInterceptors(interceptors);
        return vetClientDictionary;
    }
    @Bean
    public VetClientProduct vetClientProduct (Jaxb2Marshaller marshallerProduct){
        VetClientProduct vetClientProduct = new VetClientProduct();
        vetClientProduct.setDefaultUri(endPointProduct);
        vetClientProduct.setMarshaller(marshallerProduct);
        vetClientProduct.setUnmarshaller(marshallerProduct);
        vetClientProduct.setInterceptors(interceptors);
        return vetClientProduct;
    }

}
