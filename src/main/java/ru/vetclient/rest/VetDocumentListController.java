package ru.vetclient.rest;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.vetclient.vetclientapplications.FindVetDocument;
import wsdl.application.VetDocument;

import java.util.List;

@RestController
public class VetDocumentListController {
    private FindVetDocument findVetDocument;

    public VetDocumentListController(FindVetDocument findVetDocument) {
        this.findVetDocument = findVetDocument;
    }

    @RequestMapping ("/vetdocumentlist")
    @ResponseBody public List <VetDocument> getVetDocumentList (){
        return findVetDocument.getList();
    }

}
