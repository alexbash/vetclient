package ru.vetclient.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.vetclient.clientikarservice.VetClientIkar;
import wsdl.ikarservice.Country;

@RestController
public class IkarController {
    private VetClientIkar vetClientIkar;

    public IkarController(VetClientIkar vetClientIkar) {
        this.vetClientIkar = vetClientIkar;
    }

    @RequestMapping("/ikar/getCountryByGuidRequest")
    public @ResponseBody
    Country getCountryByGuidRequest (@RequestParam(value = "guid")String guid) throws Exception{
        return vetClientIkar.getCountryByGuidRequest(guid).getCountry();
    }
}
