package ru.vetclient.rest;

import org.springframework.web.bind.annotation.*;
import ru.vetclient.clientproductservice.VetClientProduct;
import wsdl.productservice.*;

@RestController
public class ProductController {
    private VetClientProduct vetClientProduct;

    public ProductController(VetClientProduct vetClientProduct) {
        this.vetClientProduct = vetClientProduct;
    }
    @RequestMapping("/product/getProductByGuidRequest")
    @ResponseBody public Product getProductByGuidRequest (@RequestParam(value = "guid")String guid) throws Exception{
        return vetClientProduct.getProductByGuidRequest(guid).getProduct();
    }
    @RequestMapping("/product/getSubProductByGuidRequest")
    @ResponseBody public SubProduct getSubProductByGuidRequest (@RequestParam(value = "guid")String guid) throws Exception{
        return vetClientProduct.getSubProductByGuidRequest(guid).getSubProduct();
    }
    @RequestMapping("/product/getProductItemByGuidRequest")
    @ResponseBody public ProductItem getProductItemByGuidRequest (@RequestParam(value = "guid")String guid) throws Exception{
        return vetClientProduct.getProductItemByGuidRequest(guid).getProductItem();
    }
}
