package ru.vetclient.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.vetclient.clientdictionaryservice.VetClientDictionary;
import wsdl.dictionaryservice.*;

@RestController
public class DictionaryController {
    private VetClientDictionary vetClientDictionary;

    public DictionaryController(VetClientDictionary vetClientDictionary) {
        this.vetClientDictionary = vetClientDictionary;
    }

    @RequestMapping("/dictionary/getUnitByGuidRequest")
    public @ResponseBody Unit getUnitByGuidRequest (@RequestParam(value = "guid")String guid) throws IncorrectRequestFault, InternalServiceFault, EntityNotFoundFault {
        GetUnitByGuidResponse getUnitByGuidResponse = vetClientDictionary.getUnitByGuidRequest(guid);
        return getUnitByGuidResponse.getUnit();
    }

    @RequestMapping("/dictionary/getPurposeByGuidRequest")
    public @ResponseBody Purpose getPurposeByGuidRequest (@RequestParam(value = "guid") String guid) throws IncorrectRequestFault, InternalServiceFault, EntityNotFoundFault {
        GetPurposeByGuidResponse getPurposeByGuidResponse = vetClientDictionary.getPurposeByGuidRequest(guid);
        return getPurposeByGuidResponse.getPurpose();
    }

}
