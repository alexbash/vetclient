package ru.vetclient.rest;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.vetclient.vetclientapplications.ApplicationProfile;
import ru.vetclient.vetclientapplications.ApplicationQuenchingVetDocument;
import ru.vetclient.vetclientapplications.FindVetDocument;
import ru.vetclient.vetclientapplications.VetClientApplication;
import wsdl.application.*;

@RestController
public class QuenchingController {

    private VetClientApplication vetClientApplication;
    private FindVetDocument findVetDocument;

    public QuenchingController(VetClientApplication vetClientApplication, FindVetDocument findVetDocument) {
        this.vetClientApplication = vetClientApplication;
        this.findVetDocument = findVetDocument;
    }

    @RequestMapping("/quenching")
    public @ResponseBody
    ApplicationStatus quenching(@RequestParam(value = "uuid") String uuid) throws InterruptedException, IncorrectRequestFault, UnknownServiceIdFault, AccessDeniedFault, InternalServiceFault, UnsupportedApplicationDataTypeFault {
        //TODO ApplicationProfile add for test. Delete on production
        ObjectFactory objectFactory = new ObjectFactory();
        User user = objectFactory.createUser();
        user.setLogin("bashmakov-av-170607");
        ApplicationProfile applicationProfile = ApplicationProfile.builder().apiKey("TEST_API_KEY==")
                .serviceId("mercury-g2b.service:2.0")
                .issuerID("c585d4d5-e5db-4d28-b716-a1b5f3338e47")
                .enterpriseGuid("6af2215c-0dce-4adc-978c-967920add423")
                .user(user)
                .build();
        ApplicationQuenchingVetDocument applicationQuenchingVetDocument = new ApplicationQuenchingVetDocument(vetClientApplication,applicationProfile);
	    if (findVetDocument.getVetDocumentByUuid(uuid) != null) {
           SubmitApplicationResponse submitApplicationResponse = applicationQuenchingVetDocument.applicationQuenching(findVetDocument.getVetDocumentByUuid(uuid));

           return submitApplicationResponse.getApplication().getStatus();
        } else
        return ApplicationStatus.REJECTED;
    }
}
