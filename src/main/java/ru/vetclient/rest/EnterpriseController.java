package ru.vetclient.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.vetclient.cliententerpriseservice.VetClientEnterprise;
import wsdl.dictionaryservice.EntityNotFoundFault;
import wsdl.dictionaryservice.IncorrectRequestFault;
import wsdl.dictionaryservice.InternalServiceFault;
import wsdl.enterpriseservice.BusinessEntity;
import wsdl.enterpriseservice.Enterprise;

@RestController
public class EnterpriseController {
    private VetClientEnterprise vetClientEnterprise;

    public EnterpriseController(VetClientEnterprise vetClientEnterprise) {
        this.vetClientEnterprise = vetClientEnterprise;
    }
    @RequestMapping("/enterprise/getBusinessEntityByGuidRequest")
    public @ResponseBody
    BusinessEntity getBusinessEntityByGuidRequest (@RequestParam(value = "guid")String guid) throws Exception{
        return vetClientEnterprise.getBusinessEntityByGuidRequest(guid).getBusinessEntity();
    }
    @RequestMapping("/enterprise/getEnterpriseByGuidRequest")
    public @ResponseBody
    Enterprise getEnterpriseByGuidRequest (@RequestParam(value = "guid")String guid) throws Exception {
        return vetClientEnterprise.getEnterpriseByGuidRequest(guid).getEnterprise();
    }

}
