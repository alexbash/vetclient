package ru.vetclient.clientproductservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;
import ru.vetclient.BasicAuthenticationUtil;
import wsdl.productservice.*;

public class VetClientProduct extends WebServiceGatewaySupport {

    @Value("${baseauth.user}")
    private  String user;

    @Value("${baseauth.pass}")
    private  String pass;

    //TODO All methods must be asynchronous

    public GetProductByGuidResponse getProductByGuidRequest(String guid) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault{
        GetProductByGuidRequest getProductByGuidRequest = new GetProductByGuidRequest();
        getProductByGuidRequest.setGuid(guid);
        GetProductByGuidResponse getProductByGuidResponse = (GetProductByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getProductByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        });
        return getProductByGuidResponse;
    }

    public GetSubProductByGuidResponse getSubProductByGuidRequest (String guid) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault {
        GetSubProductByGuidRequest getSubProductByGuidRequest = new GetSubProductByGuidRequest();
        getSubProductByGuidRequest.setGuid(guid);
        GetSubProductByGuidResponse getSubProductByGuidResponse = (GetSubProductByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getSubProductByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        });
        return getSubProductByGuidResponse;
    }

    public GetProductItemByGuidResponse getProductItemByGuidRequest (String guid) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault {
        GetProductItemByGuidRequest getProductItemByGuidRequest = new GetProductItemByGuidRequest();
        getProductItemByGuidRequest.setGuid(guid);
        GetProductItemByGuidResponse getProductItemByGuidResponse = (GetProductItemByGuidResponse)getWebServiceTemplate().marshalSendAndReceive(getProductItemByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        });
        return getProductItemByGuidResponse;
    }
}
