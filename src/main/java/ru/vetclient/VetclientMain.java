package ru.vetclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.vetclient.clientdictionaryservice.VetClientDictionary;
import ru.vetclient.cliententerpriseservice.VetClientEnterprise;
import ru.vetclient.clientikarservice.VetClientIkar;
import ru.vetclient.clientproductservice.VetClientProduct;
import ru.vetclient.vetclientapplications.*;
import wsdl.application.*;
import wsdl.dictionaryservice.GetUnitByGuidResponse;
import wsdl.enterpriseservice.GetBusinessEntityByGuidResponse;
import wsdl.enterpriseservice.GetEnterpriseByGuidResponse;
import wsdl.ikarservice.GetCountryByGuidResponse;
import wsdl.productservice.GetProductItemByGuidResponse;
import wsdl.productservice.GetSubProductByGuidResponse;

import java.math.BigInteger;
import java.util.List;

@SpringBootApplication
public class VetclientMain {

	public static void main(String[] args) throws ApplicationNotFoundFault, IncorrectRequestFault, InternalServiceFault, AccessDeniedFault {
		ApplicationContext ctx = SpringApplication.run(VetclientMain.class ,args);


		//Get beans
		VetClientApplication vetClientApplication = ctx.getBean(VetClientApplication.class);
		FindVetDocument findVetDocument = ctx.getBean(FindVetDocument.class);
		VetClientEnterprise vetClientEnterprise = ctx.getBean(VetClientEnterprise.class);
		VetClientIkar vetClientIkar = ctx.getBean(VetClientIkar.class);
		VetClientDictionary vetClientDictionary = ctx.getBean(VetClientDictionary.class);
		VetClientProduct vetClientProduct = ctx.getBean(VetClientProduct.class);



        // Test request VetDocumentList
        ObjectFactory objectFactory = new ObjectFactory();
        User user = objectFactory.createUser();
        user.setLogin("bashmakov-av-170607");
        ApplicationProfile applicationProfile = ApplicationProfile.builder().apiKey("TEST_API_KEY==")
                                    .serviceId("mercury-g2b.service:2.0")
                                    .issuerID("c585d4d5-e5db-4d28-b716-a1b5f3338e47")
                                    .enterpriseGuid("6af2215c-0dce-4adc-978c-967920add423")
                                    .user(user)
                                    .build();

		VetDocumentListRequest vetDocumentListRequest = new VetDocumentListRequest(vetClientApplication,applicationProfile);
		VetDocumentListResult vetDocumentListResult = new VetDocumentListResult(vetClientApplication,applicationProfile);
		String appId = "";
		try {
			appId = vetDocumentListRequest.applicationDocumentList(true, BigInteger.TEN,BigInteger.ZERO);
		}catch (Exception e){
			e.printStackTrace();
		}
		findVetDocument.setVetDocumentList(vetDocumentListResult.getVetDocumentList(appId));
		System.out.println("Кол-во " + findVetDocument.getCountVetDocument());

		//Test EnterpriseService
		try {
			GetBusinessEntityByGuidResponse getBusinessEntityByGuidResponse = vetClientEnterprise.getBusinessEntityByGuidRequest("c585d4d5-e5db-4d28-b716-a1b5f3338e47");
			GetEnterpriseByGuidResponse getEnterpriseByGuidResponse = vetClientEnterprise.getEnterpriseByGuidRequest("6af2215c-0dce-4adc-978c-967920add423");
			System.out.println("ХС владелец: " + getBusinessEntityByGuidResponse.getBusinessEntity().getInn());
			System.out.println("---------------------------------------------");
			System.out.println(getBusinessEntityByGuidResponse.getBusinessEntity().getJuridicalAddress().getAddressView());
			System.out.println("---------------------------------------------");
			System.out.println(getBusinessEntityByGuidResponse.getBusinessEntity().getFullName());
			System.out.println("---------------------------------------------");
			System.out.println(getBusinessEntityByGuidResponse.getBusinessEntity().getGuid());
			System.out.println("---------------------------------------------");
			System.out.println(getEnterpriseByGuidResponse.getEnterprise().getName());
			System.out.println("---------------------------------------------");
			System.out.println(getEnterpriseByGuidResponse.getEnterprise().getAddress().getAddressView());
		}catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("---------------------------------------------");
		//Test IkarService
		try {
			GetCountryByGuidResponse getCountryByGuidResponse = vetClientIkar.getCountryByGuidRequest("74a3cbb1-56fa-94f3-ab3f-e8db4940d96b");
			//Test DictionaryService
			GetUnitByGuidResponse getUnitByGuidResponse = vetClientDictionary.getUnitByGuidRequest("21ed96c9-337b-4a27-8761-c6e6ad3c9f5b");
			//Test ProductService
			GetSubProductByGuidResponse getSubProductByGuidResponse= vetClientProduct.getSubProductByGuidRequest("0cfe6899-94f5-0211-a66f-40f232634b42");
			System.out.println("Ikar Service:  " + getCountryByGuidResponse.getCountry().getName());
			System.out.println("---------------------------------------------");
			System.out.println("Dictionary Service:  " + getUnitByGuidResponse.getUnit().getName());
			System.out.println("---------------------------------------------");
			System.out.println("Product Service:   " + getSubProductByGuidResponse.getSubProduct().getName());
		}catch (Exception e){
			e.printStackTrace();
		}

		List <VetDocument> list = findVetDocument.getList();
		for (VetDocument doc: list) {
			System.out.print(list.indexOf(doc) + " ");
			System.out.println(doc.getCertifiedConsignment().getBatch().getProductItem().getName()
			+ " " + doc.getIssueDate() + " " + doc.getUuid() + " ");

		}



        }
}
