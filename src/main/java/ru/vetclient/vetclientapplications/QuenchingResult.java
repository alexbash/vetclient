package ru.vetclient.vetclientapplications;

import wsdl.application.*;

import javax.xml.bind.JAXBElement;
import java.util.List;

public class QuenchingResult extends ServiceApplication {


    public QuenchingResult (VetClientApplication vetClientApplication, ApplicationProfile applicationProfile) {
        super(vetClientApplication, applicationProfile);
    }

    public List <VetDocument> getQuenchingResult (String applicationID) throws ApplicationNotFoundFault, IncorrectRequestFault, InternalServiceFault, AccessDeniedFault {

        ReceiveApplicationResultResponse resultResponse = vetClientApplication.receiveApplicationResultRequest(applicationProfile.getApiKey(), applicationProfile.getIssuerID(), applicationID);

        /*Для теста*/
        System.out.println("");
        System.out.println("Статус заявки  " + resultResponse.getApplication().getStatus());

        /*Результат*/

        Application applicationResult = resultResponse.getApplication();
        ApplicationResultWrapper applicationResultWrapper = applicationResult.getResult();
        JAXBElement <ProcessIncomingConsignmentResponse> r = (JAXBElement<ProcessIncomingConsignmentResponse>) applicationResultWrapper.getAny();
        ProcessIncomingConsignmentResponse processIncomingConsignmentResponse = r.getValue();

        return processIncomingConsignmentResponse.getVetDocument();
    }

}


