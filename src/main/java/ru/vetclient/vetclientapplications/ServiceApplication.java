package ru.vetclient.vetclientapplications;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import wsdl.application.Application;
import wsdl.application.ApplicationDataWrapper;
import wsdl.application.ObjectFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;


public abstract class ServiceApplication {

    protected VetClientApplication vetClientApplication;
    protected ApplicationProfile applicationProfile;
    protected ObjectFactory objectFactory;
    protected Application application;
    protected ApplicationDataWrapper applicationDataWrapper;


    public ServiceApplication(VetClientApplication vetClientApplication, ApplicationProfile applicationProfile) {
        this.applicationProfile = applicationProfile;
        this.vetClientApplication = vetClientApplication;
        this.objectFactory = new ObjectFactory();

        /*Application*/

        application = objectFactory.createApplication();
        application.setIssuerId(applicationProfile.getIssuerID());
        application.setServiceId(applicationProfile.getServiceId());
        application.setIssueDate(gregorianCalendarDateNow());
        applicationDataWrapper = objectFactory.createApplicationDataWrapper();
    }

    public XMLGregorianCalendar gregorianCalendarDateNow(){
        GregorianCalendar calendar = new GregorianCalendar();
        return new XMLGregorianCalendarImpl(calendar);
    }
}
