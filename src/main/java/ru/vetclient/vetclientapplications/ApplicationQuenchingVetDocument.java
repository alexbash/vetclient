package ru.vetclient.vetclientapplications;

import wsdl.application.*;

import java.util.UUID;

public class ApplicationQuenchingVetDocument extends ServiceApplication {



    public ApplicationQuenchingVetDocument(VetClientApplication vetClientApplication, ApplicationProfile applicationProfile) {
        super(vetClientApplication, applicationProfile);

    }

    public SubmitApplicationResponse applicationQuenching (VetDocument vetDocument) throws InterruptedException, UnsupportedApplicationDataTypeFault, IncorrectRequestFault, UnknownServiceIdFault, InternalServiceFault, AccessDeniedFault {

        ProcessIncomingConsignmentRequest processIncomingConsignmentRequest = objectFactory.createProcessIncomingConsignmentRequest();
        Delivery delivery = objectFactory.createDelivery();
        processIncomingConsignmentRequest.setInitiator(applicationProfile.getUser());
        processIncomingConsignmentRequest.setLocalTransactionId(UUID.randomUUID().toString());
            delivery.setConsignor(vetDocument.getCertifiedConsignment().getConsignor());
            delivery.setConsignee(vetDocument.getCertifiedConsignment().getConsignee());
            delivery.setDeliveryDate(gregorianCalendarDateNow());

            //Условия приемки продукции
            DeliveryFactList deliveryFactList = objectFactory.createDeliveryFactList();
                    deliveryFactList.setVetCertificatePresence(DocumentNature.ELECTRONIC);
                DeliveryInspection deliveryInspection = objectFactory.createDeliveryInspection();
                                deliveryInspection.setResult(DeliveryInspectionResult.CORRESPONDS);
                                deliveryFactList.setDocInspection(deliveryInspection);
                                deliveryFactList.setVetInspection(deliveryInspection);
                                deliveryFactList.setDecision(DeliveryDecision.ACCEPT_ALL);

        processIncomingConsignmentRequest.setDeliveryFacts(deliveryFactList);

        //Сведения о типе ТС и способе хранения

        delivery.setTransportInfo(vetDocument.getCertifiedConsignment().getTransportInfo());
        delivery.setTransportStorageType(vetDocument.getCertifiedConsignment().getTransportStorageType());

                                   //Идентификатор ВСД
                ConsignmentDocumentList consignmentDocumentList = objectFactory.createConsignmentDocumentList();
                         consignmentDocumentList.getVetCertificate().add(vetDocument);
                         delivery.setAccompanyingForms(consignmentDocumentList);

                        //Сведения о принимаемой партии продукции.

                        Consignment consignment = objectFactory.createConsignment();
                        consignment.setProductType(vetDocument.getCertifiedConsignment().getBatch().getProductType());
                        consignment.setProduct(vetDocument.getCertifiedConsignment().getBatch().getProduct());
                        consignment.setSubProduct(vetDocument.getCertifiedConsignment().getBatch().getSubProduct());
                        consignment.setProductItem(vetDocument.getCertifiedConsignment().getBatch().getProductItem());
                        consignment.setVolume(vetDocument.getCertifiedConsignment().getBatch().getVolume());
                        consignment.setUnit(vetDocument.getCertifiedConsignment().getBatch().getUnit());
                        consignment.setDateOfProduction(vetDocument.getCertifiedConsignment().getBatch().getDateOfProduction());
                        consignment.setExpiryDate(vetDocument.getCertifiedConsignment().getBatch().getExpiryDate());
                        consignment.setPerishable(vetDocument.getCertifiedConsignment().getBatch().isPerishable());
                        consignment.getBatchID().add(vetDocument.getCertifiedConsignment().getBatch().getBatchID().get(0));
                        consignment.setOrigin(vetDocument.getCertifiedConsignment().getBatch().getOrigin());
                        consignment.setLowGradeCargo(vetDocument.getCertifiedConsignment().getBatch().isLowGradeCargo());
                        consignment.setPackageList(vetDocument.getCertifiedConsignment().getBatch().getPackageList());
                        delivery.getConsignment().add(consignment);
                        processIncomingConsignmentRequest.setDelivery(delivery);
        /*Обертка запроса */
        applicationDataWrapper.setAny(processIncomingConsignmentRequest);
        /*Application*/
        application.setData(applicationDataWrapper);
        SubmitApplicationResponse submitApplicationResponse = vetClientApplication.submitApplicationRequest(applicationProfile.getApiKey(), application);
        Thread.sleep(1000);
      return submitApplicationResponse;
    }

}
