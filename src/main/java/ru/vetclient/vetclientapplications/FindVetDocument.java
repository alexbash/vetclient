package ru.vetclient.vetclientapplications;


import org.springframework.stereotype.Component;
import wsdl.application.VetDocument;
import wsdl.application.VetDocumentList;

import java.util.List;

@Component
public class FindVetDocument {

    private VetDocumentList vetDocumentList;

    public VetDocumentList getVetDocumentList() {
        return vetDocumentList;
    }

    public void setVetDocumentList(VetDocumentList vetDocumentList) {
        this.vetDocumentList = vetDocumentList;
    }

    public List <VetDocument> getList (){
        return vetDocumentList.getVetDocument();
    }

    public int getCountVetDocument (){
        return vetDocumentList.getCount();
    }
    public VetDocument getVetDocumentByUuid (String uuid) throws NullPointerException{


        for (VetDocument doc: vetDocumentList.getVetDocument()) {
            if (doc.getUuid().equals(uuid)) return doc;
        }
        return null;
    }

}

