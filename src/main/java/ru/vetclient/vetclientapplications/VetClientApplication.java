package ru.vetclient.vetclientapplications;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;
import ru.vetclient.BasicAuthenticationUtil;
import wsdl.application.*;

import javax.xml.transform.TransformerException;
import java.io.IOException;

public class VetClientApplication extends WebServiceGatewaySupport {


    @Value("${baseauth.user}")
    private String user;

    @Value("${baseauth.pass}")
    private String pass;

    public SubmitApplicationResponse submitApplicationRequest(String apiKey, Application application)
            throws AccessDeniedFault, IncorrectRequestFault, InternalServiceFault, UnknownServiceIdFault, UnsupportedApplicationDataTypeFault {
        //Инициализация объекта запроса заявки
        SubmitApplicationRequest request = new SubmitApplicationRequest();
        request.setApiKey(apiKey);
        request.setApplication(application);
        return (SubmitApplicationResponse) getWebServiceTemplate().marshalSendAndReceive(request, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user, pass));
        });


    }

    public ReceiveApplicationResultResponse receiveApplicationResultRequest(String apiKey, String issuerId, String applicationId)
    throws AccessDeniedFault, ApplicationNotFoundFault, IncorrectRequestFault, InternalServiceFault{

        ReceiveApplicationResultRequest request = new ReceiveApplicationResultRequest();
        request.setApiKey(apiKey);
        request.setIssuerId(issuerId);
        request.setApplicationId(applicationId);
        return (ReceiveApplicationResultResponse) getWebServiceTemplate().marshalSendAndReceive(request, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user, pass));
        });
    }
}
