package ru.vetclient.vetclientapplications;

import wsdl.application.*;

import java.math.BigInteger;
import java.util.UUID;
import java.util.logging.Logger;

public class VetDocumentListRequest extends ServiceApplication {

    private static Logger log = Logger.getLogger(VetDocumentListRequest.class.getName());

    public VetDocumentListRequest(VetClientApplication vetClientApplication, ApplicationProfile applicationProfile) {
        super(vetClientApplication, applicationProfile);
    }

    //TODO must be asynchronous
    private SubmitApplicationResponse requestVetDocumentList(Application application) throws UnsupportedApplicationDataTypeFault, IncorrectRequestFault, UnknownServiceIdFault, InternalServiceFault, AccessDeniedFault {
        return vetClientApplication.submitApplicationRequest(applicationProfile.getApiKey(), application);
    }

    public String applicationDocumentList(Boolean confirmed, BigInteger count, BigInteger offset) throws InterruptedException, UnsupportedApplicationDataTypeFault, IncorrectRequestFault, UnknownServiceIdFault, InternalServiceFault, AccessDeniedFault {
        GetVetDocumentListRequest getVetDocumentListRequest = objectFactory.createGetVetDocumentListRequest();
        getVetDocumentListRequest.setEnterpriseGuid(applicationProfile.getEnterpriseGuid());
        ListOptions listOptions = objectFactory.createListOptions();
        listOptions.setCount(count);
        listOptions.setOffset(offset);
        getVetDocumentListRequest.setListOptions(listOptions);
        if (confirmed) {
            getVetDocumentListRequest.setVetDocumentStatus(VetDocumentStatus.CONFIRMED);
        } else {
            getVetDocumentListRequest.setVetDocumentStatus(VetDocumentStatus.UTILIZED);
        }
        getVetDocumentListRequest.setVetDocumentType(VetDocumentType.INCOMING);
        getVetDocumentListRequest.setInitiator(applicationProfile.getUser());
        getVetDocumentListRequest.setLocalTransactionId(UUID.randomUUID().toString());
        applicationDataWrapper.setAny(getVetDocumentListRequest);
        application.setData(applicationDataWrapper);
        SubmitApplicationResponse submitApplicationResponse = requestVetDocumentList(application);
        Thread.sleep(1000);
        log.info("Status application VetDocumentListRequest: " + submitApplicationResponse.getApplication().getStatus().value());
        return submitApplicationResponse.getApplication().getApplicationId();
    }
}
