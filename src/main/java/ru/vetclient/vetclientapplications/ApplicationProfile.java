package ru.vetclient.vetclientapplications;

import lombok.Builder;
import lombok.Getter;
import wsdl.application.User;

@Getter
public class ApplicationProfile {
    private final User user;
    private final String apiKey;
    private final String issuerID;
    private final String serviceId;
    private final String enterpriseGuid;


    @Builder
    private ApplicationProfile(User user, String apiKey, String issuerID, String serviceId, String enterpriseGuid) {
        this.user = user;
        this.apiKey = apiKey;
        this.issuerID = issuerID;
        this.serviceId = serviceId;
        this.enterpriseGuid = enterpriseGuid;
    }
}
