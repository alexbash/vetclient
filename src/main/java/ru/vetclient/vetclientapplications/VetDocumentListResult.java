package ru.vetclient.vetclientapplications;

import wsdl.application.*;

import javax.xml.bind.JAXBElement;
import java.util.logging.Logger;

public class VetDocumentListResult extends ServiceApplication {

    private static Logger logger = Logger.getLogger(VetDocumentListResult.class.getName());

    public VetDocumentListResult(VetClientApplication vetClientApplication, ApplicationProfile applicationProfile) {
        super(vetClientApplication, applicationProfile);
    }
    //TODO must be asynchronous
    private ReceiveApplicationResultResponse requestResult (String appId) throws ApplicationNotFoundFault, IncorrectRequestFault, InternalServiceFault, AccessDeniedFault {
        return vetClientApplication.receiveApplicationResultRequest(applicationProfile.getApiKey(), applicationProfile.getIssuerID(), appId);
    }

    public VetDocumentList getVetDocumentList (String applicationID) throws ApplicationNotFoundFault, IncorrectRequestFault, InternalServiceFault, AccessDeniedFault {
        ReceiveApplicationResultResponse resultResponse = requestResult(applicationID);
        logger.info("Status application vetDocumentlistResult: " + resultResponse.getApplication().getStatus());
        //Fetching result
        Application applicationResult = resultResponse.getApplication();
        ApplicationResultWrapper applicationResultWrapper = applicationResult.getResult();
        JAXBElement<GetVetDocumentListResponse> r = (JAXBElement<GetVetDocumentListResponse>) applicationResultWrapper.getAny();
        GetVetDocumentListResponse getVetDocumentListResponse = r.getValue();

        return getVetDocumentListResponse.getVetDocumentList();
    }
}
