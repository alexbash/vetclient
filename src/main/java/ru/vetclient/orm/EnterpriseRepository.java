package ru.vetclient.orm;

import org.springframework.data.repository.CrudRepository;
import ru.vetclient.orm.entity.Enterprise;

public interface EnterpriseRepository extends CrudRepository<Enterprise, Long> {
    Enterprise findByEnterpriseGuid (String enterpriseGuid);
}
