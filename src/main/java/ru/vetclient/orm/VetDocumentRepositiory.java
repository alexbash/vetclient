package ru.vetclient.orm;

import org.springframework.data.repository.CrudRepository;
import ru.vetclient.orm.entity.VetdocumentEntity;

public interface VetDocumentRepositiory extends CrudRepository<VetdocumentEntity, Long> {
}
