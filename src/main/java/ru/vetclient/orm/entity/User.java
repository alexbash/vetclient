package ru.vetclient.orm.entity;


import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Table (name = "users")
@Data
public class User implements Serializable{

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column (nullable = false)
    private String username;
    @Column
    private String fio;
    @Column (nullable = false)
    private String password;
    @ManyToOne
    @JoinColumn (name = "firm_id", referencedColumnName = "id")
    private FirmEntity firmEntity;
    @OneToMany (mappedBy = "user")
    private Collection <VetdocumentEntity> vetdocumentEntitiesById;
}
