package ru.vetclient.orm.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@Table (name = "vetdocument")
public class VetdocumentEntity implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column (nullable = false)
    private String uuid;
    @Column
    private String status;
    @Column
    private String dateFormed;
    @Column
    private String numberSafeForm;
    @Column
    private String transaction;
    @Column
    private String IncomingDocument;
    @Column
    private String productionDocument;
    @Column
    private String transport;
    @Column
    private String ttn;
    @Column
    private String target;
    @Column
    private String fioWhoFormalised;
    @Column
    private String fioWhoQuench;
    @Column
    private String notes;
    @Column
    private String specialNotes;
    @Column
    private String labResearch;
    @Column
    private String vse;
    @ManyToOne
    @JoinColumn (name = "production_id", referencedColumnName = "id")
    private ProductionEntity productionEntity;
    @ManyToOne
    @JoinColumn (name = "user_id", referencedColumnName = "id")
    private User user;
    @ManyToOne
    @JoinColumn (name = "enterprise_id", referencedColumnName = "id")
    private Enterprise enterprise;
    @ManyToOne
    @JoinColumn (name = "firm_id", referencedColumnName = "id")
    private FirmEntity firmEntity;
    @ManyToOne 
    @JoinColumn (name = "enterpriseSupplier_id", referencedColumnName = "id")
    private EnterpriseSupplierEntitiy enterpriseSupplierEntitiy;
    @ManyToOne
    @JoinColumn (name = "firmSupplier_id", referencedColumnName = "id")
    private FirmSupplierEntity firmSupplierEntity;

}
