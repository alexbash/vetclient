package ru.vetclient.orm.entity;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@Table (name = "enterpriseSupplier")
public class EnterpriseSupplierEntitiy implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private String guid;
    @Column
    private String physicalAddress;
    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable (name = "firm_enterprise", joinColumns = @JoinColumn(name = "enterprise_id"),
            inverseJoinColumns = @JoinColumn(name = "firm_id"))
    private Collection <FirmSupplierEntity> firmSupplierEntitiesById;
    @OneToMany (mappedBy = "enterpriseSupplierEntitiy")
    private Collection <VetdocumentEntity> vetdocumentEntitiesById;

}
