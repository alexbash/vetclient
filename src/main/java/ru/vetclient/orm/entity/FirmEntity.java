package ru.vetclient.orm.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Data
@Table (name = "firm")
public class FirmEntity implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column (nullable = false)
    private String issuerId;
    @Column
    private String inn;
    @Column
    private String name;
    @Column
    private String fioDirector;
    @Column
    private String address;

    @OneToMany (mappedBy = "firmEntity")
    private Collection <User> userById;
    @OneToMany (mappedBy = "firmEntity")
    private Collection <Enterprise> enterpriseById;
    @OneToMany (mappedBy = "firmEntity")
    private Collection <VetdocumentEntity> vetdocumentEntitiesById;

}
