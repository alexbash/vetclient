package ru.vetclient.orm.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@Table (name = "production")
public class ProductionEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String typeProduct;
    @Column
    String guidTypeProduct;
    @Column
    private String product;
    @Column
    private String guidProduct;
    @Column
    private String subproduct;
    @Column
    private String guidSubProduct;
    @Column
    private String productName;
    @Column
    private String guidProductName;
    @Column
    private String producer;
    @Column
    private String unit;
    @Column
    private String guidUnit;
    @Column
    private String packing;
    @Column
    private String guidPacking;
    @Column
    private String gtin;
    @Column
    private String dateMake;
    @Column
    private String dateExpiration;
    @OneToMany (mappedBy = "productionEntity")
    private Collection <VetdocumentEntity> vetdocumentEntitiesById;

}
