package ru.vetclient.orm.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Data
@Table (name = "enterprise")
public class Enterprise implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String enterpriseGuid;
    @Column
    private String fysicaladdress;
    @Column
    private String name;
    @ManyToOne
    @JoinColumn (name = "firm_id", referencedColumnName = "id")
    private FirmEntity firmEntity;
    @OneToMany (mappedBy = "enterprise")
    private Collection <VetdocumentEntity> vetdocumentEntitiesById;

}
