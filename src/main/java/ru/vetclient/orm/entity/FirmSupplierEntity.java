package ru.vetclient.orm.entity;

import com.sun.xml.internal.ws.api.server.SDDocument;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@Table (name = "firmSupplier")

public class FirmSupplierEntity implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private String inn;
    @Column
    private String fioDirector;
    @Column
    private String uridicalAddress;
    @Column
    private String guid;
    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable (name = "firm_enterprise", joinColumns = @JoinColumn(name = "firm_id"),
            inverseJoinColumns = @JoinColumn(name = "enterprise_id"))
    private Collection <EnterpriseSupplierEntitiy> enterpriseSupplierEntitiysById;
    @OneToMany (mappedBy = "firmSupplierEntity")
    private Collection <VetdocumentEntity> vetdocumentEntitiesById;


}
