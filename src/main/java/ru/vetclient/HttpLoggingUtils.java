package ru.vetclient;

import org.springframework.ws.WebServiceMessage;
import org.springframework.xml.transform.TransformerObjectSupport;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static java.util.logging.Logger.*;

public class HttpLoggingUtils extends TransformerObjectSupport {

    private static Logger log = Logger.getLogger(HttpLoggingUtils.class.getName());
    private static FileHandler fh;

    static {
        try {
            fh = new FileHandler("%h/soap%g.log", 1000000,10);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final String NEW_LINE = System.getProperty("line.separator");

    private HttpLoggingUtils() throws IOException {

    }

    public static void logMessage(String id, WebServiceMessage webServiceMessage){
        try {
            log.addHandler(fh);
            log.setUseParentHandlers(false);
            ByteArrayTransportOutputStream byteArrayTransportOutputStream = new ByteArrayTransportOutputStream();
            webServiceMessage.writeTo(byteArrayTransportOutputStream);

            String httpMessage = new String(byteArrayTransportOutputStream.toByteArray());
            log.info(NEW_LINE + "----------------------------" + NEW_LINE + id + NEW_LINE
                    + "----------------------------" + NEW_LINE + httpMessage + NEW_LINE);
        } catch (Exception e) {
            log.warning("Unable to log HTTP message." + e.getMessage());
        }

    }

}
