package ru.vetclient.clientikarservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;
import ru.vetclient.BasicAuthenticationUtil;
import wsdl.ikarservice.*;

public class VetClientIkar extends WebServiceGatewaySupport {
    @Value("${baseauth.user}")
    private String user;
    @Value("${baseauth.pass}")
    private String pass;

    //TODO All methods must be asynchronous


    public GetCountryByGuidResponse getCountryByGuidRequest (String giud) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault {
        GetCountryByGuidRequest getCountryByGuidRequest = new GetCountryByGuidRequest();
        getCountryByGuidRequest.setGuid(giud);
        GetCountryByGuidResponse getCountryByGuidResponse = (GetCountryByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getCountryByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization", BasicAuthenticationUtil.generateBasicAutenticationHeader(user, pass));
        });
        return getCountryByGuidResponse;
    }

}
