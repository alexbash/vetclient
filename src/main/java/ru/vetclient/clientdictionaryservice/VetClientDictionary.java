package ru.vetclient.clientdictionaryservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;
import ru.vetclient.BasicAuthenticationUtil;
import wsdl.dictionaryservice.*;

public class VetClientDictionary extends WebServiceGatewaySupport {

    @Value ("${baseauth.user}")
    private  String user;

    @Value("${baseauth.pass}")
    private  String pass;

    //TODO All methods must be asynchronous


    public GetUnitByGuidResponse getUnitByGuidRequest(String guid) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault {
        GetUnitByGuidRequest getUnitByGuidRequest = new GetUnitByGuidRequest();
        getUnitByGuidRequest.setGuid(guid);
        GetUnitByGuidResponse getUnitByGuidResponse = (GetUnitByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getUnitByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        });
        return getUnitByGuidResponse;
    }

    public GetPurposeByGuidResponse getPurposeByGuidRequest(String guid) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault{
        GetPurposeByGuidRequest getPurposeByGuidRequest = new GetPurposeByGuidRequest();
        getPurposeByGuidRequest.setGuid(guid);
        GetPurposeByGuidResponse getPurposeByGuidResponse = (GetPurposeByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getPurposeByGuidRequest,webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        });
        return getPurposeByGuidResponse;
    }
}
