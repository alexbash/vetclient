package ru.vetclient.cliententerpriseservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;
import ru.vetclient.BasicAuthenticationUtil;
import wsdl.dictionaryservice.EntityNotFoundFault;
import wsdl.dictionaryservice.IncorrectRequestFault;
import wsdl.dictionaryservice.InternalServiceFault;
import wsdl.enterpriseservice.GetBusinessEntityByGuidRequest;
import wsdl.enterpriseservice.GetBusinessEntityByGuidResponse;
import wsdl.enterpriseservice.GetEnterpriseByGuidRequest;
import wsdl.enterpriseservice.GetEnterpriseByGuidResponse;


public class VetClientEnterprise extends WebServiceGatewaySupport {

    @Value ("${baseauth.user}")
    private  String user;

    @Value("${baseauth.pass}")
    private  String pass;

    //TODO All methods must be asynchronous


    public GetBusinessEntityByGuidResponse getBusinessEntityByGuidRequest (String guid) throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault {
        GetBusinessEntityByGuidRequest getBusinessEntityByGuidRequest =  new GetBusinessEntityByGuidRequest();
        getBusinessEntityByGuidRequest.setGuid(guid);
        GetBusinessEntityByGuidResponse getBusinessEntityByGuidResponse = (GetBusinessEntityByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getBusinessEntityByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization",
                    BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        });
        return getBusinessEntityByGuidResponse;

    }

    public GetEnterpriseByGuidResponse getEnterpriseByGuidRequest  (String guid)throws EntityNotFoundFault, IncorrectRequestFault, InternalServiceFault{
        GetEnterpriseByGuidRequest getEnterpriseByGuidRequest = new GetEnterpriseByGuidRequest();
        getEnterpriseByGuidRequest.setGuid(guid);
        GetEnterpriseByGuidResponse getEnterpriseByGuidResponse = (GetEnterpriseByGuidResponse) getWebServiceTemplate().marshalSendAndReceive(getEnterpriseByGuidRequest, webServiceMessage -> {
            TransportContext context = TransportContextHolder.getTransportContext();
            HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
            connection.getConnection().addRequestProperty("Authorization", BasicAuthenticationUtil.generateBasicAutenticationHeader(user,pass));
        }) ;
        return getEnterpriseByGuidResponse;
    }
}

