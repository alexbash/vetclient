package ru.vetclient.Vetclient;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.vetclient.orm.EnterpriseRepository;
import ru.vetclient.orm.UserRepository;
import ru.vetclient.orm.entity.Enterprise;
import ru.vetclient.orm.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VetclientMainTests {

    @Autowired
    UserRepository userRepository;

    @Autowired
	EnterpriseRepository enterpriseRepository;

    @Test

	public void addEnterprise(){
		Enterprise enterprise = new Enterprise();

		enterprise.setEnterpriseGuid("6af2215c-0dce-4adc-978c-967920add423");
		enterprise.setName("Колхоз");
		enterprise.setFysicaladdress("Российская Федерация, Владимирская обл., г. Владимир, Большая Нижегородская ул., д. 71");
		enterpriseRepository.save(enterprise);
//		Enterprise enterprise1 = enterpriseRepository.findByEnterpriseGuid("6af2215c-0dce-4adc-978c-967920add423");
//		Assert.assertEquals("Колхоз", enterprise1.getName());
//		Assert.assertEquals("Российская Федерация, Владимирская обл., г. Владимир, Большая Нижегородская ул., д. 71", enterprise1.getFysicaladdress());
	}

	@Test
	public void findUserByUsername() {
		User user = new User();
		user.setUsername("bashmakov-av-170607");
		user.setFio("Башмаков АВ");
		user.setPassword("123456");
		userRepository.save(user);

		User user1 = userRepository.findByUsername("bashmakov-av-170607");
		Assert.assertEquals("Башмаков АВ",user1.getFio());

	}

}
